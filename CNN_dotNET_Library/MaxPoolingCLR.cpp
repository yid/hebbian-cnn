#include "MaxPoolingCLR.h"

namespace CNN_dotNET {
	MaxPooling::MaxPooling(int ReceptiveField_HEIGHT, int ReceptiveField_WIDTH) {
		Original = new CNN_SharedFiles::MaxPooling(ReceptiveField_HEIGHT, ReceptiveField_WIDTH);
	}
	MaxPooling::~MaxPooling() {
		this->!MaxPooling();
	}
	MaxPooling::!MaxPooling() {
		delete Original;
	}
}