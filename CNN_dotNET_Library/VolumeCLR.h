#pragma once
#include "Volume.h"
#include "Size3DCLR.h"

namespace CNN_dotNET {
	public ref class Volume {
	public:
		CNN_SharedFiles::Volume* Original;
		Volume(CNN_SharedFiles::Volume* Original);
		Volume(Size3D Size);
		~Volume();
		!Volume();
		property Size3D Size {Size3D get(); }
		property double default[int, int, int]{
			double get(int z,int h,int w);
			void set(int z, int h, int w, double value);
		}
	};
}