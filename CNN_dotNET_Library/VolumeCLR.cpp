#include "VolumeCLR.h"

namespace CNN_dotNET {
	Volume::Volume(CNN_SharedFiles::Volume* Original) {
		this->Original = Original;
	}
	Volume::Volume(Size3D Size) {
		Original = new CNN_SharedFiles::Volume(CNN_SharedFiles::Size3D(Size.Depth, Size.Height, Size.Width));
	}
	Volume::~Volume() {
		this->!Volume();
	}
	Volume::!Volume() {
		delete Original;
	}
	Size3D Volume::Size::get() {
		return Size3D(Original->Size.Depth, Original->Size.Height, Original->Size.Width);
	}
	double Volume::default::get(int z, int y, int x) {
		return Original->get(z, y, x);
	}
	void Volume::default::set(int z, int y, int x, double value) {
		Original->set(z, y, x, value);
	}
}