#include "ConvLayerCLR.h"

using namespace System::Runtime::InteropServices;

namespace CNN_dotNET {
	ConvLayer::ConvLayer(int KernelsNumber, int ReceptiveField_HEIGHT, int ReceptiveField_WIDTH, int Stride_X, int Stride_Y, ActivationFunctions func, ActivationFunctions func1) 
	{
		void(*Func)(double*, int);
		void(*Func1)(double*, int);
		switch (func)
		{
		case ActivationFunctions::ReLU:
			Func = CNN_SharedFiles::Layer::ReLU;
			break;
		case ActivationFunctions::Sigmoid:
			Func = CNN_SharedFiles::Layer::Sigmoid;
			break;
		case ActivationFunctions::Softmax:
			Func = CNN_SharedFiles::Layer::Softmax;
			break;
		}
		switch (func1)
		{
		case ActivationFunctions::ReLU1:
			Func1 = CNN_SharedFiles::Layer::ReLU1;
			break;
		case ActivationFunctions::Sigmoid1:
			Func1 = CNN_SharedFiles::Layer::Sigmoid1;
			break;
		case ActivationFunctions::Softmax1:
			Func1 = CNN_SharedFiles::Layer::Softmax1;
			break;
		}
		Original = new CNN_SharedFiles::ConvLayer(KernelsNumber, ReceptiveField_HEIGHT, ReceptiveField_WIDTH, Stride_X, Stride_Y,	Func, Func1);
	}
	ConvLayer::ConvLayer(System::String^ FilePath, int Stride_X, int Stride_Y, ActivationFunctions func, ActivationFunctions func1, bool TrainMode) 
	{
		const char* chars = (const char*)(Marshal::StringToHGlobalAnsi(FilePath)).ToPointer();
		string str = chars;
		Marshal::FreeHGlobal(System::IntPtr((void*)chars));

		void(*Func)(double*, int);
		void(*Func1)(double*, int);
		switch (func)
		{
		case ActivationFunctions::ReLU:
			Func = CNN_SharedFiles::Layer::ReLU;
			break;
		case ActivationFunctions::Sigmoid:
			Func = CNN_SharedFiles::Layer::Sigmoid;
			break;
		case ActivationFunctions::Softmax:
			Func = CNN_SharedFiles::Layer::Softmax;
			break;
		}
		switch (func1)
		{
		case ActivationFunctions::ReLU1:
			Func1 = CNN_SharedFiles::Layer::ReLU1;
			break;
		case ActivationFunctions::Sigmoid1:
			Func1 = CNN_SharedFiles::Layer::Sigmoid1;
			break;
		case ActivationFunctions::Softmax1:
			Func1 = CNN_SharedFiles::Layer::Softmax1;
			break;
		}

		Original = new CNN_SharedFiles::ConvLayer(str, Stride_X, Stride_Y,	Func, Func1, TrainMode);
	}
	ConvLayer::~ConvLayer() {
		this->!ConvLayer();
	}
	ConvLayer::!ConvLayer() {
		delete Original;
	}
}