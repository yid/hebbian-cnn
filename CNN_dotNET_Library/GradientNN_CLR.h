#pragma once
#include "GradientNN.h"
#include "NeuroNetCLR.h"

namespace CNN_dotNET 
{
	public ref class GradientNN :NeuroNet {
	public:
		enum class ChangeAlpha { ExponentialFunction }; // ������������ �������� ��������� ��������� �������� ��� ����������������� �������

		GradientNN(Size3D inputSize, bool TrainMode);

		virtual double TrainConstAlpha(Volume^ Target, Volume^ Input, double alpha, double max_norm); // �������� �� ������������ ���������� ��������; max_norm: ����<0, �� �� ����� ����������� max-norm �������������; ����>0, �� ����� ����������� max-norm ������������� �  �������� �������������� ������������� 

		double TrainQuasiconstAlpha(Volume^ Target, Volume^ Input, int func); // �������� � ����������������� ���������� ��������

		double TrainAdaptiveAlpha(Volume^ Target, Volume^ Input); // �������� � ���������� ����������, ������������ � ������������ � ������������������ ������������

		double TrainDichotomy(Volume^ Target, Volume^ Input); // �������� � ���������� ����������, ������������ � ������������ � ������������ ������� ���������

		void Jog(double Amplitude);
	};
}