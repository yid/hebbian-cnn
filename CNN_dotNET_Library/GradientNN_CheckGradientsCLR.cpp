#include "GradientNN_CheckGradientsCLR.h"
using namespace System::Runtime::InteropServices;

namespace CNN_dotNET
{
	GradientNN_CheckGradients::GradientNN_CheckGradients(Size3D inputSize, bool iflearn, System::String^ path):GradientNN(Size3D(inputSize.Depth, inputSize.Height, inputSize.Width), iflearn)
	{
		const char* chars = (const char*)(Marshal::StringToHGlobalAnsi(path)).ToPointer();
		string str = chars;
		Marshal::FreeHGlobal(System::IntPtr((void*)chars));
		Original = new CNN_SharedFiles::GradientNN_CheckGradients(CNN_SharedFiles::Size3D(inputSize.Depth, inputSize.Height, inputSize.Width), iflearn, str);
	}
}