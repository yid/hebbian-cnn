#pragma once

#include "FullConnectedLayer.h"
#include "LayerCLR.h"

namespace CNN_dotNET {
	public ref class FullConnectedLayer : Layer {
	public:
		delegate double Function(double);

		FullConnectedLayer(int NeuronsNumber, ActivationFunctions func, ActivationFunctions func1);
		FullConnectedLayer(System::String^ FilePath, ActivationFunctions func, ActivationFunctions func1, bool iflearn);
		~FullConnectedLayer();
		!FullConnectedLayer();
	};
}