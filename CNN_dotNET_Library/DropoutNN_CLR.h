#pragma once
#include "DropoutNN.h"
#include "GradientNN_CLR.h"
namespace CNN_dotNET
{
	public ref class DropoutNN :
		public GradientNN
	{
	public:
		void AddToDropoutNN(Layer^ nextlayer, double nextp);

		void FinishTraining();

		DropoutNN(Size3D inputSize, bool iflearn);
	};
}

