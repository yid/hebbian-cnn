#pragma once
#include "GradientNN_CheckGradients.h"
#include "GradientNN_CLR.h"

namespace CNN_dotNET
{
	public ref class GradientNN_CheckGradients :GradientNN
	{
	public:
		GradientNN_CheckGradients(Size3D inputSize, bool iflearn, System::String^ path);
	};
}