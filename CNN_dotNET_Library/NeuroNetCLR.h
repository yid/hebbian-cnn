#pragma once
#include "NeuroNet.h"
#include "LayerCLR.h"

namespace CNN_dotNET {
	public ref class NeuroNet {
	private:
		Volume^ Input;
	protected:
		CNN_SharedFiles::NeuroNet* Original;
		NeuroNet();
		System::Collections::Generic::List<Layer^>^ Layers;
	public:
		NeuroNet(Size3D inputSize, bool iflearn);
		void Add(Layer^ nextLayer);
		virtual Volume^ Forward(Volume^ Input);
		void Save(System::String^ FolderPath);
		~NeuroNet();
		!NeuroNet();
	};
}