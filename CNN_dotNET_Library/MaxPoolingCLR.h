#pragma once
#include "MaxPooling.h"
#include "LayerCLR.h"

namespace CNN_dotNET {
	public ref class MaxPooling : public Layer {
	public:
		MaxPooling(int ReceptiveField_HEIGHT, int ReceptiveField_WIDTH);
		~MaxPooling();
		!MaxPooling();
	};
}