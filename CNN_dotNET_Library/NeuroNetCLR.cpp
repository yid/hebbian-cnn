#include "NeuroNetCLR.h"
using namespace System::Runtime::InteropServices;

namespace CNN_dotNET {
	NeuroNet::NeuroNet() {
		Layers = gcnew System::Collections::Generic::List<Layer^>();
	}
	NeuroNet::NeuroNet(Size3D inputSize, bool TrainMode):NeuroNet() {
		Original = new CNN_SharedFiles::NeuroNet(CNN_SharedFiles::Size3D(inputSize.Depth, inputSize.Height, inputSize.Width), TrainMode);
	}
	void NeuroNet::Add(Layer^ nextLayer) {
		Layers->Add(nextLayer);
		Original->Add(nextLayer->Original);
	}
	Volume^ NeuroNet::Forward(Volume^ Input) {
		CNN_SharedFiles::Volume* OriginalResult = Original->Forward(Input->Original);
		CNN_SharedFiles::Volume* OriginalResultCopy = new CNN_SharedFiles::Volume(OriginalResult->Size);
		for (int i = 0; i < OriginalResult->Size.Depth*OriginalResult->Size.Height*OriginalResult->Size.Width; i++)
			OriginalResultCopy->pMass[i] = OriginalResult->pMass[i];
		Volume^ Result = gcnew Volume(OriginalResultCopy);
		return Result;
	}
	NeuroNet::~NeuroNet() {
		this->!NeuroNet();
	}
	NeuroNet::!NeuroNet() {
		delete Original;
	}
	void NeuroNet::Save(System::String^ FolderPath) {
		const char* chars = (const char*)(Marshal::StringToHGlobalAnsi(FolderPath)).ToPointer();
		string str = chars;
		Marshal::FreeHGlobal(System::IntPtr((void*)chars));
		Original->Save(str);
	}
}