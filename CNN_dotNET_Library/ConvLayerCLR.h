#pragma once
#include "ConvLayer.h"
#include "LayerCLR.h"

namespace CNN_dotNET {
	public ref class ConvLayer : Layer {
	public:
		ConvLayer(int KernelsNumber, int ReceptiveField_HEIGHT, int ReceptiveField_WIDTH, int Stride_X, int Stride_Y, ActivationFunctions func, ActivationFunctions func1);
		ConvLayer(System::String^ FilePath, int Stride_X, int Stride_Y, ActivationFunctions func, ActivationFunctions func1, bool TrainMode);
		~ConvLayer();
		!ConvLayer();
	};
}