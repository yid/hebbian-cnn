#pragma once
#include "Layer.h"
#include "VolumeCLR.h"

namespace CNN_dotNET {
	public ref class Layer abstract{
	public:
		CNN_SharedFiles::Layer* Original = 0;
		property Size3D outputSize {
			Size3D get() {
				if (Original != 0)
					return Size3D(Original->outputSize.Depth, Original->outputSize.Height, Original->outputSize.Width);
				else
					return Size3D(0, 0, 0);
			}
		};

		enum class ActivationFunctions{Sigmoid, Sigmoid1, ReLU, ReLU1, Softmax, Softmax1};
	};
}