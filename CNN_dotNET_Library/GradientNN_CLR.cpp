#include "GradientNN_CLR.h"

namespace CNN_dotNET 
{
	GradientNN::GradientNN(Size3D inputSize, bool TrainMode){
		Original = new CNN_SharedFiles::GradientNN(CNN_SharedFiles::Size3D(inputSize.Depth, inputSize.Height, inputSize.Width), TrainMode);
	}

	double GradientNN::TrainConstAlpha(Volume^ Target, Volume^ Input, double alpha, double max_norm) 
	{
		return dynamic_cast<CNN_SharedFiles::GradientNN*>(Original)->TrainConstAlpha(Target->Original, Input->Original, alpha, max_norm);
	}

	double GradientNN::TrainQuasiconstAlpha(Volume^ Target, Volume^ Input, int func) {
		return dynamic_cast<CNN_SharedFiles::GradientNN*>(Original)->TrainQuasiConstAlpha(Target->Original, Input->Original, (CNN_SharedFiles::GradientNN::ChangeAlpha)func);
	}

	double GradientNN::TrainAdaptiveAlpha(Volume^ Target, Volume^ Input) {
		return dynamic_cast<CNN_SharedFiles::GradientNN*>(Original)->TrainQuasiAnaliticAlpha(Target->Original, Input->Original);
	}
	double GradientNN::TrainDichotomy(Volume^ Target, Volume^ Input) {
		return dynamic_cast<CNN_SharedFiles::GradientNN*>(Original)->TrainDichotomy(Target->Original, Input->Original);
	}
	void GradientNN::Jog(double Amplitude) {
		dynamic_cast<CNN_SharedFiles::GradientNN*>(Original)->Jog(Amplitude);
	}
}