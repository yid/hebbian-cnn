#pragma once

namespace CNN_dotNET {
	public value class Size3D {
	public:
		int Width, Height, Depth;
		Size3D(int Depth, int Height, int Width) {
			this->Depth = Depth;
			this->Height = Height;
			this->Width = Width;
		}
	};
}