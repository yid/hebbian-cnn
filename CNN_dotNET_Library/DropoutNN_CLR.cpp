#include "DropoutNN_CLR.h"


namespace CNN_dotNET
{
	DropoutNN::DropoutNN(Size3D inputSize, bool TrainMode) :GradientNN(Size3D(inputSize.Depth, inputSize.Height, inputSize.Width), TrainMode)
	{
		Original = new CNN_SharedFiles::DropoutNN(CNN_SharedFiles::Size3D(inputSize.Depth, inputSize.Height, inputSize.Width), TrainMode);
	}

	void DropoutNN::AddToDropoutNN(Layer^ nextLayer, double nextp) 
	{
		Layers->Add(nextLayer);
		dynamic_cast<CNN_SharedFiles::DropoutNN*>(Original)->AddToDropoutNN(nextLayer->Original, nextp);
	}

	void DropoutNN::FinishTraining()
	{
		dynamic_cast<CNN_SharedFiles::DropoutNN*>(Original)->FinishTraining();
	}
}
