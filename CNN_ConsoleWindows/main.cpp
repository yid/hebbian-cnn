﻿#include"NeuroNet.h"
#include "GradientNN.h"
#include "HebbianNN.h"
#include<fstream>
#include<iostream>
using namespace std;
using namespace CNN_SharedFiles;
#include "ConvLayer.h"
#include "FullConnectedLayer.h"
#include "MaxPooling.h"
#include "GradientNN_CheckGradients.h"
#include "DropoutNN.h"
#include "cblas.h"
#include <random>
#include <ctime>
#include <cstdio>
#include <typeinfo>
#include <cmath>
#include "DatasetParser.h"
#include "windows.h"
#include "Padding.h"

bool DEBUG = false;
const string mnist_images_path = "C:\\Users\\yid\\Downloads\\mnist\\train-images.idx3-ubyte";
const string mnist_labels_path = "C:\\Users\\yid\\Downloads\\mnist\\train-labels.idx1-ubyte";

const string mnist_test_images_path = "C:\\Users\\yid\\Downloads\\mnist\\t10k-images.idx3-ubyte";
const string mnist_test_labels_path = "C:\\Users\\yid\\Downloads\\mnist\\t10k-labels.idx1-ubyte";

const string cifar10_test_path = "C:\\Users\\yid\\Downloads\\cifar-10-batches-bin\\test_batch.bin";
string cifar10_train_paths[5] = { "C:\\Users\\yid\\Downloads\\cifar-10-batches-bin\\data_batch_1.bin",
"C:\\Users\\yid\\Downloads\\cifar-10-batches-bin\\data_batch_2.bin",
"C:\\Users\\yid\\Downloads\\cifar-10-batches-bin\\data_batch_3.bin",
"C:\\Users\\yid\\Downloads\\cifar-10-batches-bin\\data_batch_4.bin",
"C:\\Users\\yid\\Downloads\\cifar-10-batches-bin\\data_batch_5.bin" };

int mnist_test_images_number = 10000;
int mnist_train_images_number = 60000;
int cifar10_train_images_number = 50000;
int cifar10_test_images_number = 10000;


int main(int argc, char **argv)
{
	srand(time(NULL));
	DatasetParser* parser = new DatasetParser();

#pragma region Preparing MNIST
	cout << "Preparing MNIST..." << endl;
	parser->read_mnist(mnist_images_path, mnist_labels_path, mnist_train_images_number);
	Volume** mnist_inputs = parser->MNISTImages;
	Volume** mnist_answers = parser->MNISTAnswers;
	parser->read_mnist(mnist_test_images_path, mnist_test_labels_path, mnist_test_images_number);
	Volume** mnist_testInputs = parser->MNISTImages;
	Volume** mnist_testAnswers = parser->MNISTAnswers;
	cout << "Prepared." << endl;

	cout << "Saving 10th image from MNIST to mnist_example.bmp..." << endl;
	parser->VolumeToBitmap(mnist_inputs[9], "mnist_example.bmp");
	cout << "Saved." << endl;
#pragma endregion

#pragma region Preparing CIFAR10
	cout << "Preparing CIFAR10..." << endl;
	// note, that here we consider the case when the training dataset is stored to 5 files
	Volume*** inputs_arr = new Volume**[5];
	Volume*** answers_arr = new Volume**[5];
	int temp = cifar10_train_images_number / 5;
	for (int i = 0; i < 5; i++)
	{
		parser->read_cifar10(cifar10_train_paths[i], temp);
		inputs_arr[i] = parser->CIFAR10Images;
		answers_arr[i] = parser->CIFAR10Answers;
	}
	Volume** cifar10_inputs = new Volume*[cifar10_train_images_number];
	Volume** cifar10_answers = new Volume*[cifar10_train_images_number];
	for (int i = 0; i < cifar10_train_images_number; i++)
		for (int j = 0; j < temp; j++)
		{
			cifar10_inputs[i] = inputs_arr[i / temp][j];
			cifar10_answers[i] = answers_arr[i / temp][j];
		}
	parser->read_cifar10(cifar10_test_path, cifar10_test_images_number);
	Volume** cifar10_testInputs = parser->CIFAR10Images;
	Volume** cifar10_testAnswers = parser->CIFAR10Answers;
	for (int i = 0; i < 5; i++)
	{
		delete[] inputs_arr[i];
		delete[] answers_arr[i];
	}
	delete[] inputs_arr;
	delete[] answers_arr;
	cout << "Prepared." << endl;

	cout << "Saving 10th image from CIFAR10 to cifar10_example.bmp..." << endl;
	parser->VolumeToBitmap(cifar10_inputs[9], "cifar10_example.bmp");
	cout << "Saved." << endl;
#pragma endregion


	// later on we operate with cifar10 dataset
	int inputSize = 32 * 32; // input size of image in CIFAR10 dataset
	int outputSize = 10; // number of classes in CIFAR10 dataset
	int num_iter = 10; // number of learning iterations

	vector<string> Names;
	vector<NeuroNet*> NNs;
	vector<HebbianNN::TrainingRule> TRules;
	vector<HebbianNN::SupervisedRule> SRules;
	vector<double> Alphas;

	double tempAlpha;

#pragma region Neural network architectures
	cout << "Creating neural networks..." << endl;

	NeuroNet* NN;
	//hebbian
	tempAlpha = 0.00001;
	NN = new HebbianNN(Size3D(1, 32, 32), true);

	NN->Add(new ConvLayer(32, 3, 3, 2, 2, Layer::Linear, Layer::Linear1));
	NN->Add(new ConvLayer(32, 3, 3, 1, 1, Layer::Linear, Layer::Linear1));
	NN->Add(new Padding(2, 2));

	NN->Add(new ConvLayer(64, 3, 3, 2, 2, Layer::Linear, Layer::Linear1));
	NN->Add(new ConvLayer(64, 3, 3, 1, 1, Layer::Linear, Layer::Linear1));
	NN->Add(new Padding(2, 2));

	NN->Add(new FullConnectedLayer(outputSize, Layer::Softmax, Layer::Softmax1));// output 10
	Names.push_back("Hebbian");
	NNs.push_back(NN);
	TRules.push_back(HebbianNN::TrainingRule::Oja_n);
	SRules.push_back(HebbianNN::SupervisedRule::GradientLast);
	Alphas.push_back(tempAlpha);

	//gradient
	tempAlpha = 0.00001;
	NN = new GradientNN(Size3D(1, 32, 32), true);

	NN->Add(new ConvLayer(32, 3, 3, 2, 2, Layer::Linear, Layer::Linear1));
	NN->Add(new ConvLayer(32, 3, 3, 1, 1, Layer::Linear, Layer::Linear1));
	NN->Add(new Padding(2, 2));

	NN->Add(new ConvLayer(64, 3, 3, 2, 2, Layer::Linear, Layer::Linear1));
	NN->Add(new ConvLayer(64, 3, 3, 1, 1, Layer::Linear, Layer::Linear1));
	NN->Add(new Padding(2, 2));

	NN->Add(new FullConnectedLayer(outputSize, Layer::Softmax, Layer::Softmax1));// output 10
	Names.push_back("Gradient");
	NNs.push_back(NN);
	TRules.push_back(HebbianNN::TrainingRule::Oja_n); // actually this makes sense only for hebbian nn, so we here we can assign any values, they won't be use in training anyway
	SRules.push_back(HebbianNN::SupervisedRule::GradientLast); // the same for this parameter
	Alphas.push_back(tempAlpha);

	cout << "Created." << endl;
#pragma endregion

	cout << "Training begins..." << endl;
	for (int i = 0; i < num_iter; i++)
	{
		if (i % 100 == 0)
			cout << i << " of " << num_iter << endl;
		int ind = rand() % cifar10_train_images_number;
		for (int j = 0; j < NNs.size(); j++)
		{
			double err;
			if (typeid(*NNs[j]) == typeid(HebbianNN))
				err = dynamic_cast<HebbianNN*>(NNs[j])->TrainConstAlpha(cifar10_inputs[ind], cifar10_answers[ind], Alphas[j], TRules[j], SRules[j], -1);
			if (typeid(*NNs[j]) == typeid(GradientNN))
				err = dynamic_cast<GradientNN*>(NNs[j])->TrainConstAlpha(cifar10_answers[ind], cifar10_inputs[ind], Alphas[j], -1);
		}
	}
	cout << "Training finished. Evaluation and saving weights..." << endl;
	for (int j = 0; j < NNs.size(); j++)
	{
		string Name(Names[j].begin(), Names[j].end());
		double error = NNs[j]->Evaluate_l1(cifar10_testInputs, cifar10_testAnswers, cifar10_test_images_number);
		cout << Name << endl << error << endl;
		NNs[j]->Save(to_string(j));
	}
cout << "Finished." << endl;
getchar();
return 0;
}