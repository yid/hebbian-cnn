# Hebbian CNN

It is an educational project, containing implementation "from scratch" for convolutional, fully-connected neural networks and with supervised (gradient) and unsurpervised (hebbian) learning using high-performance low-level library CBLAS. This project was being developed from 2015 to 2017 by Dmirtii Rashchenko (dimitree54@gmail.com) and Iuliia Rashchenko (july.rashchenko@gmail.com). It was a base for Iuliia Rashchenko's master thesis, for details see [the link](https://dspace.spbu.ru/bitstream/11701/12215/1/Rashhenko_YU.pdf).

All logic is implemented in C++ as matrices operations, with the means of OOP and high-performance low-level library CBLAS, but without using any high-level specialized machine learning frameworks. For more details of what is implemented observe section [Content review](https://gitlab.com/yid/hebbian-cnn/blob/master/README.md#content-review) It can be useful as an educational task for beginners in neural networks to understand main processes of them deeper. However for some real-world projects it is strongly recommended to use some specialized frameworks like Tensorflow.

# Requirements
1. CBLAS ([link](http://www.netlib.org/blas/));
2. C++ Bitmap Library for processing routines for the 24-bit per pixel bitmap image format (it has been already added to repository as file "bitmap_image.hpp", can be downloaded [here](http://www.partow.net/programming/bitmap/)).

# Content review
1.  Size3D.h, Size4D.h, Volume.h, Volume4D.h and their source code "cpp" files: auxiliary classes for representation of data tensors transmitting inside networks.
2.  Layer.h, FullConnectedLayer.h, ConvLayer.h, Padding.h, MaxPooling.h, FullConnectedLayer_LateralInhibition.h and corresponding cpp files: classes for specific layers of neural network.
3.  NeuroNet.h, GradientNN.h, DropoutNN.h, HebbianNN.h, GradientNN_CheckGradients.h and corresponding cpp files: classes for different neural network architectures. GradientNN is the main class for creating convolutional and fully-connected networks being trained with gradient descent optimization.
4.  DatasetParser.h and its cpp file is supplementary class for parsing cifar10 and mnist datasets binary files and transforming them into format convenient for processing within this project.
5.  main.cpp: source file with examples of creating and training networks using provided API 

# Datasets
In examples we use cifar10 and mnist datasets, you can download them from official sources ([MNIST](http://yann.lecun.com/exdb/mnist/index.html), [CIFAR10](http://www.cs.toronto.edu/~kriz/cifar.html)). Note, that our code is configured to process binary version of cifar10 dataset.

# License
This project is developed by Dmitrii Rashchenko and Iuliia Rashchenko within 2015-2017 and distributed under the MIT license (see file of LICENSE for more details).