﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CNN_MakeLayers
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = "";
            Console.WriteLine("Enter height, width of input, number of kernels, height, width of kernel, height-stride, width-stride.");
            s = Console.ReadLine();
            while (!s.Equals("bye"))
            {
                string[] smas = s.Split(' ');
                int m1 = int.Parse(smas[0]);
                int n1 = int.Parse(smas[1]);
                int k2 = int.Parse(smas[2]);
                int h = int.Parse(smas[3]);
                int w = int.Parse(smas[4]);
                int th = int.Parse(smas[5]);
                int tw = int.Parse(smas[6]);
                Console.WriteLine("Get depth, height and width of output.");
                Console.WriteLine(k2.ToString() + " " + ((m1 - h) / th + 1).ToString() + " " + ((n1 - w) / tw + 1).ToString());
                Console.WriteLine("Enter height, width of input, number of kernels, height, width of kernel, height-stride, width-stride.");
                s = Console.ReadLine();
            }
        }
    }
}
