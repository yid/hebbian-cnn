#include "Volume4D.h"
#include "Size4D.h"

namespace CNN_SharedFiles
{
	Volume4D::Volume4D(Size4D Size)
	{
		this->Size = Size;
		pMass = new double[Size.Depth4D*Size.Depth*Size.Height*Size.Width];
	}

	Volume4D::~Volume4D()
	{
		delete[] pMass;
	}
	double Volume4D::get(int t, int z, int y, int x)
	{
		return pMass[t*Size.Depth*Size.Width*Size.Height + z*Size.Width*Size.Height + y*Size.Width + x];
	}
	void Volume4D::set(int t, int z, int y, int x, double value)
	{
		pMass[t*Size.Depth*Size.Width*Size.Height + z*Size.Width*Size.Height + y*Size.Width + x] = value;
	}
	Volume4D::Volume4D(const Volume4D & v)
	{
		Size = v.Size;
		pMass = new double[Size.Depth4D*Size.Depth*Size.Height*Size.Width];
#pragma omp parallel for
		for (int i = 0; i < Size.Depth4D*Size.Depth*Size.Height*Size.Width; i++)
			pMass[i] = v.pMass[i];
	}
}
