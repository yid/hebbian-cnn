#pragma once
#include "Volume.h"
#include "Size4D.h"
namespace CNN_SharedFiles
{ // ����� ������������ ���������� ��� �������� � ������ � 4-������� ���������
	class Volume4D
	{
	public:
		Size4D Size;
		double * pMass;

		Volume4D(Size4D Size);
		~Volume4D();
		double get(int t, int z, int y, int x);
		void set(int t, int z, int y, int x, double value);
		Volume4D(const Volume4D & v);
	};
}

