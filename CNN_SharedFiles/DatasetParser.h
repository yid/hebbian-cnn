#include <string>
#include "Volume.h"
using namespace std;


namespace CNN_SharedFiles
{
	// supplementary class for parsing cifar10 and mnist datasets binary files and transforming them into format convenient for processing within this project
	class DatasetParser
	{
	private:
		const int cifar10_image_depth = 3;
		const int cifar10_image_height = 32;
		const int cifar10_image_width = 32;
		const int cifar10_classes_number = 10;
		const int mnist_image_depth = 1;
		const int mnist_image_height = 28;
		const int mnist_image_width = 28;
		const int mnist_classes_number = 10;

		// converts "char*" 1D-array to "Volume*" 1D-array of size "array_size", where each element has depth=width=1, height=number_of_classes and is in fact a vector where "Volume"[i][0,j,0]=1, if "char"[i]=j, and "Volume"[i][0,j,0]=0 otherwise.
		Volume** Char1DToVolume1D(unsigned char* arr, int array_size, int number_of_classes);
		// converts "char**" 2D-array to "Volume**" 1D-array; each row in "array" contains pixels of an image of size "height*width" for "depth" channels ("height*width" pixels for the 1st channel and so on) devided by 255; "array_size" is number of rows; these parameters are used for making "Volume**".
		Volume** Char4DToVolume1D(unsigned char** arr, int array_size, int depth, int height, int width);
	public:
		// dynamic array of "Volume*", each of them is a 3-channel image
		Volume** CIFAR10Images;
		// dynamic array of "Volume*", each of them has size 1*10*1 and is in fact a vector with one '1' and all other elements '0's.
		Volume** CIFAR10Answers;
		// dynamic array of "Volume*", each of them is a 3-channel image
		Volume** MNISTImages;
		// dynamic array of "Volume*", each of them has size 1*10*1 and is in fact a vector with one '1' and all other elements '0's.
		Volume** MNISTAnswers;
		string* cifar10_names;

		DatasetParser();
		~DatasetParser();

		// reads "number_of_images" mnist images from file "full_path" and mnist labels from file "full_path_labels" and writes them to "MNISTImages" and "MNISTLabels" respectively, deleting previous information from them before that; all files must be in "idx3-ubyte" format
		void read_mnist(string full_path_images, string full_path_labels, int& number_of_images);

		/**
		reads "number_of_images" cifar10 images and cifar10 labels from the file "full_path" 
		and writes them to fields "cifar10_images" and "cifar10_labels" accordingly, deleting 
		previous iformation from them before that; the file must be in "bin" format
		*/
		void read_cifar10(string full_path, int& number_of_images);

		/**
		transforms Volume* structure to bmp
		@param volume input structure
		@param path where to save
		@return 1
		*/
		int VolumeToBitmap(Volume* volume, string path);
	};
}