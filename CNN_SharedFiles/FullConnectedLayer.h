﻿#pragma once
#include "Layer.h"
#include <string>
using namespace std;

namespace CNN_SharedFiles
{
	// класс полносвязного слоя, дочерний для класса Layer;
	class FullConnectedLayer :public Layer
	{

	protected:
		void(*ActivationFunction)(double* x, int size); // активационная функция слоя
		void(*ActivationFunction1)(double* x, int size); // производная активационной функции слоя
		Volume* BackError; // ошибка, распространяющаяся на предыдущий слой

	public:
		Volume* UnactivatedOutput; // неактивированный выход слоя

		Volume* Output; // выход слоя

		Volume* Weights; // матрица, в строках которой веса одного нейрона

		FullConnectedLayer(int NeuronsNumber, void(*ActivationFunction)(double* x, int size), void(*ActivationFunction1)(double* x, int size));

		virtual bool Connect(Size3D InputSize, bool iflearn);

		virtual Volume* Forward(Volume* Input);

		virtual Volume* Backward(Volume* Error);

		~FullConnectedLayer();

		void Save(string folder, int layer_number);

		FullConnectedLayer(string s, void(*ActivationFunction)(double* x, int size), void(*ActivationFunction1)(double* x, int size), bool iflearn);

		FullConnectedLayer(const FullConnectedLayer & f);

		Layer* Copy();
	};
}

