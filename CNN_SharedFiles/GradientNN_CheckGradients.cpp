#include "GradientNN_CheckGradients.h"
#include <iostream>
#include <fstream>
#include "cblas.h"
#include <typeinfo>
#include <cmath>
using namespace std;

namespace CNN_SharedFiles
{
	GradientNN_CheckGradients::GradientNN_CheckGradients(Size3D inputSize, bool iflearn, string path) :
		GradientNN(inputSize, iflearn)
	{
		this->Path = path;
	}


	GradientNN_CheckGradients::~GradientNN_CheckGradients()
	{
	}

	double GradientNN_CheckGradients::TrainConstAlpha(Volume * Target, Volume * Input, double alpha)
	{
		ALPHA = alpha;
		double Error = 0;
		if (!iflearn)
		{
			cout << "This NeuroNet is not for learning.";
			return 0;
		}
		Forward(Input);
		for (int i = 0; i < Output->Size.Height; i++)
			Error += abs(Output->get(0, i, 0) - Target->get(0, i, 0));

		Volume* ErrorForBackward = new Volume(Output->Size);
		for (int i = 0; i < Output->Size.Height; i++)
			ErrorForBackward->set(0, i, 0, Target->get(0, i, 0) - Output->get(0, i, 0));
		Backward(ErrorForBackward);
		ofstream file(Path);
		file.close();
		for (size_t i = 0; i < Layers.size(); i++)
		{
			ofstream file(Path, ofstream::app);
			file << "layer " << i << endl;
			file.close();
			if (typeid(*Layers[i]) == typeid(ConvLayer))
				TrainConvLayer(dynamic_cast<ConvLayer*>(Layers[i]));
			if (typeid(*Layers[i]) == typeid(FullConnectedLayer))
				TrainFCLayer(dynamic_cast<FullConnectedLayer*>(Layers[i]));
		}
		delete ErrorForBackward;
		return Error;
	}

	void GradientNN_CheckGradients::TrainConvLayer(ConvLayer * layer)
	{
		// параллельное умножение: Wights:=Weights+ETA*Error*TransformedInput
		cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, layer->K, layer->S, layer->L, ALPHA, layer->Error->pMass, layer->L, layer->TransformedInput, layer->S, 1, layer->Weights->pMass, layer->S);
		Volume4D* Temp = new Volume4D(layer->Weights->Size);
		cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, layer->K, layer->S, layer->L, 1, layer->Error->pMass, layer->L, layer->TransformedInput, layer->S, 0, Temp->pMass, layer->S);
		ofstream file(Path, ofstream::app);
		for (int i = 0; i<layer->K*layer->S; i++)
			file << Temp->pMass[i] << endl;
		file.close();
		delete Temp;
	}

	void GradientNN_CheckGradients::TrainFCLayer(FullConnectedLayer * layer)
	{
		int height = layer->outputSize.Height;
		int width = layer->InputSize.Depth*layer->InputSize.Height*layer->InputSize.Width;
		// параллельное умножение: Wights:=Weights+ETA*Error*Input^T
		cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, height, width, 1, ALPHA, layer->Error->pMass, 1, layer->Input->pMass, 1, 1, layer->Weights->pMass, width);
		Volume * Temp = new Volume(layer->Weights->Size);
		cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, height, width, 1, 1, layer->Error->pMass, 1, layer->Input->pMass, 1, 0, Temp->pMass, width);
		ofstream file(Path, ofstream::app);
		for (int i = 0; i < width*height; i++)
			file << Temp->pMass[i] << endl;
		file.close();
		delete Temp;
	}
}