﻿#include <iostream>
using namespace std;
#include "ConvLayer.h"
#include "cblas.h"
#include <fstream>
#include <string>

namespace CNN_SharedFiles
{
	ConvLayer::ConvLayer(int KernelsNumber, int ReceptiveField_HEIGHT, int ReceptiveField_WIDTH, int Stride_X, int Stride_Y, void(*ActivationFunction)(double* x, int size), void(*ActivationFunction1)(double* x, int size))
	{
		outputSize.Depth = KernelsNumber;
		this->ReceptiveField_HEIGHT = ReceptiveField_HEIGHT;
		this->ReceptiveField_WIDTH = ReceptiveField_WIDTH;
		this->Stride_X = Stride_X;
		this->Stride_Y = Stride_Y;
		this->ActivationFunction = ActivationFunction;
		this->ActivationFunction1 = ActivationFunction1;
	}

	ConvLayer::ConvLayer(string s, int Stride_X, int Stride_Y, void(*ActivationFunction)(double* x, int size), void(*ActivationFunction1)(double* x, int size), bool iflearn)
	{
		ifstream in(s);

		in >> outputSize.Depth;
		in >> this->ReceptiveField_HEIGHT;
		in >> this->ReceptiveField_WIDTH;
		this->Stride_X = Stride_X;
		this->Stride_Y = Stride_Y;
		this->ActivationFunction = ActivationFunction;
		this->ActivationFunction1 = ActivationFunction1;

		int inputdepth;
		in >> inputdepth;
		Layer::Connect(Size3D(inputdepth, 0, 0), iflearn);

		K = outputSize.Depth;
		S = InputSize.Depth * ReceptiveField_HEIGHT * ReceptiveField_WIDTH;

		Weights = new Volume4D(Size4D(outputSize.Depth, InputSize.Depth, ReceptiveField_HEIGHT, ReceptiveField_WIDTH));
		for (int i = 0; i < K*S; i++)
			in >> Weights->pMass[i];

		in.close();
	}

	bool ConvLayer::Connect(Size3D InputSize, bool iflearn)
	{
		if (ifconnect)
		{
			if (this->InputSize.Depth != InputSize.Depth)
				return false;
			else
			{
				this->InputSize.Height = InputSize.Height;
				this->InputSize.Width = InputSize.Width;
			}
		}
		else
		{
			Layer::Connect(InputSize, iflearn);
			double a = -0.1, b = 0.1;
			K = outputSize.Depth;
			S = InputSize.Depth * ReceptiveField_HEIGHT * ReceptiveField_WIDTH;
			Weights = new Volume4D(Size4D(outputSize.Depth, InputSize.Depth, ReceptiveField_HEIGHT, ReceptiveField_WIDTH));
#pragma omp parallel for
			for (int i = 0; i < K*S; i++)
				Weights->pMass[i] = (b - a)*rand() / RAND_MAX + a;
		}

		outputSize.Height = (InputSize.Height - ReceptiveField_HEIGHT) / Stride_Y + 1;
		if (outputSize.Height < 1)
			outputSize.Height = 1;
		outputSize.Width = (InputSize.Width - ReceptiveField_WIDTH) / Stride_X + 1;
		if (outputSize.Width < 1)
			outputSize.Width = 1;
		Output = new Volume(outputSize);
		L = outputSize.Height * outputSize.Width;
		TransformedInput = new double[L * S];
		UnactivatedOutput = new Volume(outputSize);
		if (iflearn)
		{
			BackError = new Volume(InputSize);
		}
		return true;
	}

	Volume * ConvLayer::Forward(Volume * Input)
	{
		this->Input = Input;

#pragma omp parallel for
		for (int i = 0; i < outputSize.Height; i++)
			for (int j = 0; j < outputSize.Width; j++)
				for (int k = 0; k < InputSize.Depth; k++)
					for (int l = 0; l < ReceptiveField_HEIGHT; l++)
						for (int m = 0; m < ReceptiveField_WIDTH; m++)
						{
							TransformedInput[i*ReceptiveField_WIDTH*ReceptiveField_HEIGHT*InputSize.Depth*outputSize.Width + j*ReceptiveField_WIDTH*ReceptiveField_HEIGHT*InputSize.Depth + k*ReceptiveField_WIDTH*ReceptiveField_HEIGHT + l*ReceptiveField_WIDTH + m] = Input->get(k, i*Stride_Y + l, j*Stride_X + m);
						}
		// параллельное умножение. UnactivatedOut = Weigths*(Temp^T)
		cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, K, L, S, 1.0, Weights->pMass, S, TransformedInput, S, 0.0, UnactivatedOutput->pMass, L);

#pragma omp parallel for
		for (int i = 0; i < outputSize.Depth*outputSize.Height*outputSize.Width; i++)
			Output->pMass[i] = UnactivatedOutput->pMass[i];
		ActivationFunction(Output->pMass, outputSize.Depth*outputSize.Height*outputSize.Width);
		return Output;
	}

	Volume * ConvLayer::Backward(Volume * Error)
	{
		this->Error = Error;
		//Error=Error*f'(UnactivatedOutput) поэлементно
		double* Temp0 = new double[outputSize.Depth*outputSize.Height*outputSize.Width];
#pragma omp parallel for
		for (int i = 0; i < outputSize.Depth*outputSize.Height*outputSize.Width; i++)
			Temp0[i] = UnactivatedOutput->pMass[i];
		ActivationFunction1(Temp0, outputSize.Depth*outputSize.Height*outputSize.Width);
#pragma omp parallel for
		for (int i = 0; i < outputSize.Depth*outputSize.Height*outputSize.Width; i++)
			Error->pMass[i] = Error->pMass[i] * Temp0[i];
		delete Temp0;

		if (FirstLayer)
			return 0;
		else
		{
			// vector which is analogous to "TransformedInput".
			double* Temp = new double[L * S];
			// Параллельное умножение: Temp = (Error^T)*Weights
			cblas_dgemm(CblasRowMajor, CblasTrans, CblasNoTrans, L, S, K, 1.0, Error->pMass, L, Weights->pMass, S, 0.0, Temp, S);
#pragma omp parallel for
			for (int i = 0; i < InputSize.Depth; i++)
				for (int j = 0; j < InputSize.Height; j++)
					for (int k = 0; k < InputSize.Width; k++)
						BackError->set(i, j, k, 0);

			// procedure opposite to  transformation from "Input" to "TransformedInput" in function "Forward".
#pragma omp parallel for
			for (int k = 0; k < InputSize.Depth; k++)
				for (int i = 0; i < outputSize.Height; i++)
					for (int j = 0; j < outputSize.Width; j++)
						for (int l = 0; l < ReceptiveField_HEIGHT; l++)
							for (int m = 0; m < ReceptiveField_WIDTH; m++)
								BackError->set(k, i*Stride_Y + l, j*Stride_X + m, BackError->get(k, i*Stride_Y + l, j*Stride_X + m) + Temp[i*ReceptiveField_WIDTH*ReceptiveField_HEIGHT*InputSize.Depth*outputSize.Width + j*ReceptiveField_WIDTH*ReceptiveField_HEIGHT*InputSize.Depth + k*ReceptiveField_WIDTH*ReceptiveField_HEIGHT + l*ReceptiveField_WIDTH + m]);
			delete[] Temp;
			return BackError;
		}
	}

	ConvLayer::~ConvLayer()
	{
		delete[] TransformedInput;
		delete Output;
		delete UnactivatedOutput;
		if (ifconnect)
		{
			delete Weights;
		}
		if (iflearn)
		{
			delete BackError;
		}
	}
	void ConvLayer::Save(string folder, int layer_number)
	{
		string full_address = "";

		if (layer_number < 10)
			full_address = folder + '0' + to_string(layer_number) + "CL.cnn";
		else
			full_address = folder + to_string(layer_number) + "CL.cnn";

		ofstream out(full_address);
		out << outputSize.Depth << endl << this->ReceptiveField_HEIGHT << endl << this->ReceptiveField_WIDTH << endl << InputSize.Depth << endl;
		for (int i = 0; i < K*S; i++)
			out << Weights->pMass[i] << endl;
		out.close();
	}
	ConvLayer::ConvLayer(const ConvLayer & c) :Layer(c)
	{
		this->ActivationFunction = c.ActivationFunction;
		this->ActivationFunction1 = c.ActivationFunction1;
		this->K = c.K;
		this->L = c.L;
		this->S = c.S;
		this->ReceptiveField_HEIGHT = c.ReceptiveField_HEIGHT;
		this->ReceptiveField_WIDTH = c.ReceptiveField_WIDTH;
		this->Stride_X = c.Stride_X;
		this->Stride_Y = c.Stride_Y;

		if (iflearn)
			BackError = new Volume(InputSize);

		Output = new Volume(outputSize);
		TransformedInput = new double[L * S];
		UnactivatedOutput = new Volume(outputSize);

		Weights = new Volume4D(Size4D(outputSize.Depth, InputSize.Depth, ReceptiveField_HEIGHT, ReceptiveField_WIDTH));
#pragma omp parallel for
		for (int i = 0; i < K*S; i++)
			Weights->pMass[i] = c.Weights->pMass[i];
	}
	Layer * ConvLayer::Copy()
	{
		Layer* l = new ConvLayer(*this);
		return l;
	}
}