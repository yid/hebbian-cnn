﻿#pragma once
#include"Size3D.h"
#include"Volume.h"
#include <string>
using namespace std;

namespace CNN_SharedFiles
{ 
	// базовый абстрактный класс Нейронный слой
	class Layer
	{
	public:
		Size3D outputSize; // размер выхода слоя
		bool FirstLayer = false; // флаг, обозначающий, является ли слой первым
		Size3D InputSize; // размер входа слоя
		Volume * Input = 0; // вход слоя
		Volume * Error = 0; // ошибка слоя

		virtual Volume* Forward(Volume* Input) = 0; // функция прямого распространения
		virtual Volume* Backward(Volume* Error) = 0; // функция обратного распространения
		
		virtual bool Connect(Size3D InputSize, bool iflearn);// функция, связывающая данный слой с предыдущим слоем
		Layer();
		virtual void Save(string folder, int layer_number) = 0; // функция сохранения слоя
		Layer(const Layer & l);
		virtual Layer* Copy()=0;
		virtual ~Layer() {}

		// функции активации и их производные

		static void Sigmoid(double* x, int size);
		static void Sigmoid1(double* x, int size);
		static void Sigmoid_Antisimmetric(double* x, int size);
		static void Sigmoid_Antisimmetric1(double* x, int size);
		static void ReLU(double* x, int size);
		static void ReLU1(double* x, int size);
		static void Softmax(double* x, int size);
		static void Softmax1(double* x, int size);
		static void Linear(double* x, int size);
		static void Linear1(double* x, int size);

		static void HardMax(double * x, int size);

	protected:
		bool iflearn; // флаг, определяющий режим функционирования (обучение или работа)
		bool ifconnect; // флаг, определяющий, вызывалась ли функция Connect
	};
}

