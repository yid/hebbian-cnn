﻿#pragma once
#include "HebbianNN.h"
#include "ConvLayer.h"
#include <iostream>
using namespace std;
#include <cmath>
#include <typeinfo>
#include "cblas.h"

// TODO think of parallelism
namespace CNN_SharedFiles
{
	HebbianNN::HebbianNN(const HebbianNN & g) :NeuroNet(g)
	{
		this->ALPHA = 1;
	}

	void HebbianNN::TrainFCLayer(FullConnectedLayer* layer, TrainingRule rule, double MAX_NORM)
	{
		switch (rule)
		{
		case TrainingRule::Oja_1:
			TrainFCLayerOja_1(layer, false);
			break;
		case TrainingRule::Oja_n:
			TrainFCLayerOja_n(layer, false, false);
			break;
		case TrainingRule::Oja_n_theory:
			TrainFCLayerOja_n(layer, false, true);
			break;
		case TrainingRule::Oja_1_Activated:
			TrainFCLayerOja_1(layer, true);
			break;
		case TrainingRule::Oja_n_Activated:
			TrainFCLayerOja_n(layer, true, false);
			break;
		case TrainingRule::Oja_n_theory_Activated:
			TrainFCLayerOja_n(layer, true, true);
			break;
			/*case TrainingRule::APEX:
				if (typeid(*layer) == typeid(FullConnectedLayer_LateralInhibition)) {
					TrainFCLateralLayerAPEX(dynamic_cast<FullConnectedLayer_LateralInhibition*>(layer), false);
					break;

				}*/
		case TrainingRule::APEX_Activated:
			if (typeid(*layer) == typeid(FullConnectedLayer_LateralInhibition)) {
				TrainFCLateralLayerAPEX(dynamic_cast<FullConnectedLayer_LateralInhibition*>(layer), true);
				break;
			}
		default:
			TrainFCLayerHebb(layer, MAX_NORM);
			break;
		}
	}

	void HebbianNN::TrainFCLayerHebb(FullConnectedLayer* layer, bool Activated, double MAX_NORM)
	{
		int height = layer->outputSize.Height;
		int width = layer->InputSize.Depth*layer->InputSize.Height*layer->InputSize.Width;
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
			{
				double input = layer->Input->pMass[j];
				double output;
				if (Activated)
					output = layer->Output->pMass[i];
				else
					output = layer->UnactivatedOutput->pMass[i];
				layer->Weights->pMass[i*width + j] += ALPHA * input * output;
			}

		if (MAX_NORM > 0)
		{
			double norm = getNorm(layer->Weights->pMass, height*width);
			double k = MAX_NORM / norm;
			double* A = new double[height * 1];
			double* B = new double[1 * width];
			cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, height, width, 1, 0, A, 1, B, width, k, layer->Weights->pMass, width);
			delete A;
			delete B;
		}
	}

	void HebbianNN::TrainFCLayerOja_1(FullConnectedLayer* layer, bool Activated)
	{
		int height = layer->outputSize.Height;
		int width = layer->InputSize.Depth*layer->InputSize.Height*layer->InputSize.Width;
		for (int i = 0; i < height; i++)
			for (int j = 0; j < width; j++)
			{
				double w = layer->Weights->pMass[i*width + j];
				double input = layer->Input->pMass[j];
				double output;
				if (Activated)
					output = layer->Output->pMass[i];
				else
					output = layer->UnactivatedOutput->pMass[i];
				layer->Weights->pMass[i*width + j] += ALPHA * output * (input - w * output);
			}
	}

	void HebbianNN::TrainFCLayerOja_n(FullConnectedLayer* layer, bool Activated, bool theory)
	{
		int height = layer->outputSize.Height;
		int width = layer->InputSize.Depth*layer->InputSize.Height*layer->InputSize.Width;

		double sum;
		if (theory) {
			for (int j = 0; j < width; j++)
			{
				sum = 0;
				for (int i = 0; i < height; i++)
				{
					double w = layer->Weights->pMass[i*width + j];
					double input = layer->Input->pMass[j];
					double output;
					if (Activated)
						output = layer->Output->pMass[i];
					else
						output = layer->UnactivatedOutput->pMass[i];
					sum += w * output;
					layer->Weights->pMass[i*width + j] += ALPHA * output * (input - sum);
				}
			}
			int ii = 0; ii++;
		}
		else
		{ // можно запараллелить на blas
			for (int j = 0; j < width; j++)
			{
				sum = 0;
				for (int k = 0; k < height; k++)
					if (Activated)
						sum += layer->Output->pMass[k] * layer->Weights->get(0, k, j);
					else
						sum += layer->UnactivatedOutput->pMass[k] * layer->Weights->get(0, k, j);
				for (int i = 0; i < height; i++)
				{
					double w = layer->Weights->pMass[i*width + j];
					double input = layer->Input->pMass[j];
					double output;
					if (Activated)
						output = layer->Output->pMass[i];
					else
						output = layer->UnactivatedOutput->pMass[i];
					layer->Weights->pMass[i*width + j] += ALPHA * output * (input - sum);
				}
			}
		}
	}

	void HebbianNN::TrainConvLayer(ConvLayer * layer, TrainingRule rule, double MAX_NORM)
	{
		switch (rule)
		{
		case TrainingRule::Oja_1:
			TrainConvLayerOja_1(layer, MAX_NORM);
			break;
		case TrainingRule::Oja_n:
			TrainConvLayerOja_n(layer, false, false);
			break;
		case TrainingRule::Oja_n_Activated:
			TrainConvLayerOja_n(layer, true, false);
			break;
		case TrainingRule::Oja_n_theory:
			TrainConvLayerOja_n(layer, false, true);
			break;
		case TrainingRule::Oja_n_theory_Activated:
			TrainConvLayerOja_n(layer, true, true);
			break;
		default:
			TrainConvLayerHebb(layer, MAX_NORM);
			break;
		}
	}

	void HebbianNN::TrainConvLayerHebb(ConvLayer * layer, double MAX_NORM)
	{
		int K2 = layer->outputSize.Depth;
		int K1 = layer->InputSize.Depth;
		int H = layer->ReceptiveField_HEIGHT;
		int W = layer->ReceptiveField_WIDTH;
		int M2 = layer->outputSize.Height;
		int N2 = layer->outputSize.Width;

#pragma omp parallel for
		for (int k2 = 0; k2 < K2; k2++)
			for (int k1 = 0; k1 < K1; k1++)
				for (int h = 0; h < H; h++)
					for (int w = 0; w < W; w++)
					{
						double delta = 0;
						for (int m2 = 0; m2 < M2; m2++)
							for (int n2 = 0; n2 < N2; n2++)
							{
								delta += layer->TransformedInput[m2*N2*K1*H*W + n2*K1*H*W + k1*H*W + h*W + w] * layer->Output->get(k2, m2, n2);
							}
						delta /= M2*N2;
						layer->Weights->set(k2, k1, h, w, layer->Weights->get(k2, k1, h, w) + ALPHA*delta);
					}

		if (MAX_NORM > 0)
		{
			double norm = getNorm(layer->Weights->pMass, layer->K*layer->S);
			double k = MAX_NORM / norm;
			double* A = new double[layer->K * 1];
			double* B = new double[1 * layer->S];
			cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, layer->K, layer->S, 1, 0, A, 1, B, layer->S, k, layer->Weights->pMass, layer->S);
			delete A;
			delete B;
		}
	}

	void HebbianNN::TrainConvLayerOja_1(ConvLayer * layer, double MAX_NORM)
	{
		int K2 = layer->outputSize.Depth;
		int K1 = layer->InputSize.Depth;
		int H = layer->ReceptiveField_HEIGHT;
		int W = layer->ReceptiveField_WIDTH;
		int M2 = layer->outputSize.Height;
		int N2 = layer->outputSize.Width;

#pragma omp parallel for
		for (int k2 = 0; k2 < K2; k2++)
			for (int k1 = 0; k1 < K1; k1++)
				for (int h = 0; h < H; h++)
					for (int w = 0; w < W; w++)
					{
						double weights = layer->Weights->get(k2, k1, h, w);
						double delta = 0;
						for (int m2 = 0; m2 < M2; m2++)
							for (int n2 = 0; n2 < N2; n2++)
							{
								double output = layer->Output->get(k2, m2, n2);
								double input = layer->TransformedInput[m2*N2*K1*H*W + n2*K1*H*W + k1*H*W + h*W + w];
								delta += output*(input - weights*output);
							}
						delta /= M2*N2;
						layer->Weights->set(k2, k1, h, w, layer->Weights->get(k2, k1, h, w) + ALPHA*delta);
					}

		if (MAX_NORM > 0)
		{
			double norm = getNorm(layer->Weights->pMass, layer->K*layer->S);
			double k = MAX_NORM / norm;
			double* A = new double[layer->K * 1];
			double* B = new double[1 * layer->S];
			cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, layer->K, layer->S, 1, 0, A, 1, B, layer->S, k, layer->Weights->pMass, layer->S);
			delete A;
			delete B;
		}
	}

	void HebbianNN::TrainConvLayerOja_n(ConvLayer * layer, bool Activated, bool Theory)
	{
		int K2 = layer->outputSize.Depth;
		int K1 = layer->InputSize.Depth;
		int H = layer->ReceptiveField_HEIGHT;
		int W = layer->ReceptiveField_WIDTH;
		int M2 = layer->outputSize.Height;
		int N2 = layer->outputSize.Width;

		Volume4D* deltas = new Volume4D(layer->Weights->Size);
		for (int i = 0; i < deltas->Size.Depth4D*deltas->Size.Depth*deltas->Size.Height*deltas->Size.Width; i++)
		{
			deltas->pMass[i] = 0;
		}

		if (!Theory)
		{
#pragma omp parallel for
			for (int k2 = 0; k2 < K2; k2++)
				for (int k1 = 0; k1 < K1; k1++)
					for (int h = 0; h < H; h++)
						for (int w = 0; w < W; w++)
						{
							double weights = layer->Weights->get(k2, k1, h, w);
							for (int m2 = 0; m2 < M2; m2++)
								for (int n2 = 0; n2 < N2; n2++)
								{
									double output;
									if (Activated)
										output = layer->Output->get(k2, m2, n2);
									else
										output = layer->UnactivatedOutput->get(k2, m2, n2);
									double input = layer->TransformedInput[m2*N2*K1*H*W + n2*K1*H*W + k1*H*W + h*W + w];
									double sum = 0;
									for (int a = 0; a < K2; a++)
										sum += layer->Output->get(a, m2, n2) * layer->Weights->get(a, k1, h, w);
									deltas->set(k2, k1, h, w, deltas->get(k2, k1, h, w) + output*(input - sum));
								}
							deltas->set(k2, k1, h, w, deltas->get(k2, k1, h, w) / (M2*N2));
						}

#pragma omp parallel for
			for (int i = 0; i < layer->K*layer->S; i++)
			{
				layer->Weights->pMass[i] += ALPHA*deltas->pMass[i];
			}
			delete deltas;
		}
		else {
			cout << "Oja_n_theory is not implemented!";
			throw exception("NotImplemented");
		}
	}

	void HebbianNN::TrainFCLayerGradient(FullConnectedLayer * layer, Volume * error, double MAX_NORM)
	{
		layer->Backward(error);

		int height = layer->outputSize.Height;
		int width = layer->InputSize.Depth*layer->InputSize.Height*layer->InputSize.Width;
		// параллельное умножение: Wights:=Weights+ETA*Error*Input^T
		cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, height, width, 1, ALPHA, layer->Error->pMass, 1, layer->Input->pMass, 1, 1, layer->Weights->pMass, width);
		if (MAX_NORM > 0)
		{
			double norm = getNorm(layer->Weights->pMass, height*width);
			double k = MAX_NORM / norm;
			double* A = new double[height * 1];
			double* B = new double[1 * width];
			cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, height, width, 1, 0, A, 1, B, width, k, layer->Weights->pMass, width);
			delete A;
			delete B;
		}
	}

	double HebbianNN::TrainConstAlpha(Volume* Input, Volume* Target, double alpha, TrainingRule rule, SupervisedRule Srule, double MAX_NORM)
	{
		//this->MAX_NORM = maxnorm;
		ALPHA = alpha;
		double Error = 0;
		if (!iflearn)
		{
			cout << "This NeuroNet is not for learning.";
			return 0;
		}
		Forward(Input);
		for (int i = 0; i < Output->Size.Height; i++)
			Error += abs(Output->get(0, i, 0) - Target->get(0, i, 0));

		bool PositiveDirection = false;
		if (Srule == SupervisedRule::DirectionAll || Srule == SupervisedRule::DirectionLast) {
			int MaxIndexTarget = 0, MaxIndexOutput = 0;
			for (int i = 1; i < Output->Size.Height; i++) {
				if (Output->get(0, i, 0) > Output->get(0, MaxIndexOutput, 0))
					MaxIndexOutput = i;
				if (Target->get(0, i, 0) > Target->get(0, MaxIndexTarget, 0))
					MaxIndexTarget = i;
			}
			if (MaxIndexOutput == MaxIndexTarget)
				PositiveDirection = true;
		}

		for (size_t i = 0; i < Layers.size(); i++)
		{
			switch (Srule)
			{
			case SupervisedRule::TargetedLast:
				if (i == Layers.size() - 1) {
					if (typeid(*Layers[i]) == typeid(FullConnectedLayer))
						TrainFCLayer_SupervisedWithTarget(dynamic_cast<FullConnectedLayer*>(Layers[i]), Target, rule, MAX_NORM);
					break;
				}
			case SupervisedRule::DirectionLast:
				if (i == Layers.size() - 1)
				{
					if (typeid(*Layers[i]) == typeid(FullConnectedLayer))
						TrainFCLayer_SupervisedWithDirection(dynamic_cast<FullConnectedLayer*>(Layers[i]), PositiveDirection, rule, MAX_NORM);
					break;
				}
			case SupervisedRule::DirectionAll:
				if (typeid(*Layers[i]) == typeid(FullConnectedLayer))
					TrainFCLayer_SupervisedWithDirection(dynamic_cast<FullConnectedLayer*>(Layers[i]), PositiveDirection, rule, MAX_NORM);
				break;
			case SupervisedRule::GradientLastDirectionAll:
				if (i == Layers.size() - 1)
				{
					if (typeid(*Layers[i]) == typeid(FullConnectedLayer))
					{
						Volume* ErrorForBackward = new Volume(Output->Size);
						for (int i = 0; i < Output->Size.Height; i++)
							ErrorForBackward->set(0, i, 0, Target->get(0, i, 0) - Output->get(0, i, 0));
						TrainFCLayerGradient(dynamic_cast<FullConnectedLayer*>(Layers[i]), ErrorForBackward, MAX_NORM);
						delete ErrorForBackward;
					}
				}
				else
				{
					if (typeid(*Layers[i]) == typeid(FullConnectedLayer))
						TrainFCLayer_SupervisedWithDirection(dynamic_cast<FullConnectedLayer*>(Layers[i]), PositiveDirection, rule, MAX_NORM);
				}
				break;
			case SupervisedRule::GradientLast:
				if (i == Layers.size() - 1)
				{
					if (typeid(*Layers[i]) == typeid(FullConnectedLayer))
					{
						Volume* ErrorForBackward = new Volume(Output->Size);
						for (int i = 0; i < Output->Size.Height; i++)
							ErrorForBackward->set(0, i, 0, Target->get(0, i, 0) - Output->get(0, i, 0));
						TrainFCLayerGradient(dynamic_cast<FullConnectedLayer*>(Layers[i]), ErrorForBackward, MAX_NORM);
						delete ErrorForBackward;
					}
					break;
				}
			default:
				if (typeid(*Layers[i]) == typeid(ConvLayer))
					TrainConvLayer(dynamic_cast<ConvLayer*>(Layers[i]), rule, MAX_NORM);
				if (typeid(*Layers[i]) == typeid(FullConnectedLayer))
					TrainFCLayer(dynamic_cast<FullConnectedLayer*>(Layers[i]), rule, MAX_NORM);
				if (typeid(*Layers[i]) == typeid(FullConnectedLayer_LateralInhibition))
					TrainFCLayer(dynamic_cast<FullConnectedLayer_LateralInhibition*>(Layers[i]), rule, MAX_NORM);
				break;
			}
		}
		return Error;
	}



	HebbianNN::HebbianNN(Size3D inputSize, bool iflearn) :NeuroNet(inputSize, iflearn)
	{
		ALPHA = 1;
	}


	HebbianNN::~HebbianNN()
	{
	}


	void HebbianNN::TrainFCLateralLayerAPEX(FullConnectedLayer_LateralInhibition* layer, bool Activated)
	{
		HebbianNN::TrainFCLayerOja_1(layer, Activated);

		int height = layer->outputSize.Height;
		int width = layer->outputSize.Height;
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++)
			{
				if (j > i) {
					double w = layer->LateralWeights[i*width + j];
					double input = layer->Output->pMass[j];
					double output = layer->Output->pMass[i];
					layer->LateralWeights[i*width + j] -= ALPHA * output * (input + w * output); // ВАЖНО! в скобке должен быть плюс, иначе - неустойчивость
				}
			}
		}
	}

	void HebbianNN::TrainFCLayer_SupervisedWithDirection(FullConnectedLayer* layer, bool PositiveDirection, TrainingRule rule, double MAX_NORM)
	{
		if (!PositiveDirection) {
			ALPHA *= -1;
		}
		TrainFCLayer(layer, rule, MAX_NORM);
		if (!PositiveDirection) {
			ALPHA *= -1;
		}
	}
	void HebbianNN::TrainFCLayer_SupervisedWithTarget(FullConnectedLayer* layer, Volume* Target, TrainingRule rule, double MAX_NORM) {
		Volume* RealOutput = layer->Output;
		layer->Output = Target;
		TrainFCLayer(layer, rule, MAX_NORM);
		layer->Output = RealOutput;
	}
}