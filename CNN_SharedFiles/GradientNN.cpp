﻿#include "GradientNN.h"
#include <iostream>
#include <string>
#include <typeinfo>
#include "ConvLayer.h"
#include "FullConnectedLayer.h"
#include "cblas.h"
#include <cmath>
using namespace std;

namespace CNN_SharedFiles
{
	double GradientNN::Exponential(double x)
	{
		return (0.1 + 0.4 * exp(-0.0001*x));
	}

	GradientNN::GradientNN(Size3D inputSize, bool iflearn) :NeuroNet(inputSize, iflearn)
	{
		ALPHA = 1;
		TrainCounter = 0;
	}

	GradientNN::~GradientNN()
	{
	}

	double GradientNN::TrainConstAlpha(Volume* Target, Volume* Input, double alpha, double maxnorm)
	{
		this->MAX_NORM = maxnorm;
		ALPHA = alpha;
		double Error = 0;
		if (!iflearn)
		{
			cout << "This NeuroNet is not for learning.";
			return 0;
		}
		Forward(Input);
		for (int i = 0; i < Output->Size.Height; i++)
			Error += abs(Output->get(0, i, 0) - Target->get(0, i, 0));

		Volume* ErrorForBackward = new Volume(Output->Size);
		for (int i = 0; i < Output->Size.Height; i++)
			ErrorForBackward->set(0, i, 0, Target->get(0, i, 0) - Output->get(0, i, 0));
		Backward(ErrorForBackward);

		for (size_t i = 0; i < Layers.size(); i++)
		{
			if (typeid(*Layers[i]) == typeid(ConvLayer))
				TrainConvLayer(dynamic_cast<ConvLayer*>(Layers[i]));
			if (typeid(*Layers[i]) == typeid(FullConnectedLayer))
				TrainFCLayer(dynamic_cast<FullConnectedLayer*>(Layers[i]));
		}
		delete ErrorForBackward;
		return Error;
	}

	double GradientNN::TrainQuasiConstAlpha(Volume * Target, Volume * Input, ChangeAlpha func)
	{
		double(*Func)(double);
		switch (func)
		{
		case ExponentialFunction:
			Func = Exponential;
			break;
		}
		TrainCounter++;
		return TrainConstAlpha(Target, Input, Func(TrainCounter), this->MAX_NORM);
	}

	double GradientNN::TrainQuasiAnaliticAlpha(Volume * Target, Volume * Input)
	{
		if (!iflearn)
		{
			cout << "This NeuroNet is not for learning.";
			return 0;
		}

		double alphas[3] = { 0,0.5,1 };
		double errors[3] = { 0,0,0 };

		for (int i = 0; i < 3; i++)
		{
			GradientNN * copy = new GradientNN(*this);
			if (alphas[i] != 0)
			{
				copy->TrainConstAlpha(Target, Input, alphas[i], this->MAX_NORM);
			}
			copy->Forward(Input);
			for (int j = 0; j < copy->Output->Size.Height; j++)
				errors[i] += abs(copy->Output->get(0, j, 0) - Target->get(0, j, 0));
			delete copy;
		}

		// рассчитываем коэффициенты параболы по трем точкам, формулы взяты из интернета
		double a_min = -1, a, b, c;
		if (alphas[2] * (alphas[2] - alphas[1] - alphas[0]) + alphas[0] * alphas[1] != 0)
		{
			a = (errors[2] - (alphas[2] * (errors[1] - errors[0]) + alphas[1] * errors[0] - alphas[0] * errors[1]) / (alphas[1] - alphas[0])) / (alphas[2] * (alphas[2] - alphas[1] - alphas[0]) + alphas[0] * alphas[1]);
			b = (errors[1] - errors[0]) / (alphas[1] - alphas[0]) - a*(alphas[0] + alphas[1]);
			c = (alphas[1] * errors[0] - alphas[0] * errors[1]) / (alphas[1] - alphas[0]) + a*alphas[0] * alphas[1];
			// нахождение экстремума функции
			a_min = -b / (2 * a);
		}
		if (a <= 0)
		{
			double e_min = errors[0];
			int i_min = 0;
			for (int i = 1; i < 3; i++)
			{
				if (errors[i] < e_min)
				{
					e_min = errors[i];
					i_min = i;
				}
			}
			ALPHA = alphas[i_min];
		}
		else
			if (a_min < 0)
			{
				ALPHA = 0;
			}
			else
				if (a_min > 5)
				{
					ALPHA = 5;
				}
				else
					ALPHA = a_min;

		// обучение самой нейросети с выбранным параметром обучения
		return this->TrainConstAlpha(Target, Input, ALPHA, this->MAX_NORM);
	}

	double GradientNN::TrainDichotomy(Volume * Target, Volume * Input)
	{
		if (!iflearn)
		{
			cout << "This NeuroNet is not for learning.";
			return 0;
		}
		double Eps = 0.1;
		double a = 0.1;
		double b = 1.5;
		double N = 0;// количество итераций
		double y[2] = { 0,0 };
		// проводим оптимизацию методом дихотомии
		do
		{
			double delta = (b - a) / 4;
			double x[2] = { (a + b) / 2 - delta , (a + b) / 2 + delta };
			// вычисляем значение ошибки при каждом значении альфа
			for (int i = 0; i < 2; i++)
			{
				GradientNN * copy = new GradientNN(*this);
				if (x[i] != 0)
				{
					copy->TrainConstAlpha(Target, Input, x[i], this->MAX_NORM);
				}
				copy->Forward(Input);
				for (int j = 0; j < copy->Output->Size.Height; j++)
					y[i] += abs(copy->Output->get(0, j, 0) - Target->get(0, j, 0));
				delete copy;
			}
			// определяем новые границы отрезка
			if (y[0] > y[1])
				a = x[0];
			else
				b = x[1];
			N++;
		} while ((abs(y[0] - y[1]) >= Eps) && (N < 30));
		// обучение самой нейросети с выбранным параметром обучения

		if (y[0] > y[1])
			return this->TrainConstAlpha(Target, Input, b, this->MAX_NORM);
		else
			return this->TrainConstAlpha(Target, Input, a, this->MAX_NORM);
	}

	void GradientNN::Jog(double x)
	{
		for (size_t i = 0; i < Layers.size(); i++)
		{
			int size = 0;
			ConvLayer * cl;
			FullConnectedLayer * fcl;
			if (cl = dynamic_cast<ConvLayer*>(Layers[i]))
			{
				size = (cl->K)*(cl->S);
				for (int j = 0; j < size; j++)
					cl->Weights->pMass[j] += 2 * x*rand() / RAND_MAX - x;
			}
			if (fcl = dynamic_cast<FullConnectedLayer*>(Layers[i]))
			{
				size = (fcl->Weights->Size.Depth)*(fcl->Weights->Size.Height)*(fcl->Weights->Size.Width);
				for (int j = 0; j < size; j++)
					fcl->Weights->pMass[j] += 2 * x*rand() / RAND_MAX - x;
			}
		}
	}

	GradientNN::GradientNN(const GradientNN & g) :NeuroNet(g)
	{
		this->ALPHA = 1;
		this->TrainCounter = g.TrainCounter;
	}

	void GradientNN::TrainConvLayer(ConvLayer* layer)
	{
		if (this->MAX_NORM < 0)
		{
			// параллельное умножение: Wights:=Weights+ETA*Error*TransformedInput
			cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, layer->K, layer->S, layer->L, ALPHA, layer->Error->pMass, layer->L, layer->TransformedInput, layer->S, 1, layer->Weights->pMass, layer->S);
		}
		else
		{
			cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, layer->K, layer->S, layer->L, ALPHA, layer->Error->pMass, layer->L, layer->TransformedInput, layer->S, 1, layer->Weights->pMass, layer->S);
			double norm = getNorm(layer->Weights->pMass, layer->K*layer->S);

			//if (norm > MAX_NORM)
			//{
				double k = MAX_NORM / norm;
				double* A = new double[layer->K * 1];
				double* B = new double[1 * layer->S];
				cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, layer->K, layer->S, 1, 0, A, 1, B, layer->S, k, layer->Weights->pMass, layer->S);
				delete A;
				delete B;
			//}
		}
	}
	void GradientNN::TrainFCLayer(FullConnectedLayer* layer)
	{
		int height = layer->outputSize.Height;
		int width = layer->InputSize.Depth*layer->InputSize.Height*layer->InputSize.Width;
		if (this->MAX_NORM < 0)
		{
			// параллельное умножение: Wights:=Weights+ETA*Error*Input^T
			cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, height, width, 1, ALPHA, layer->Error->pMass, 1, layer->Input->pMass, 1, 1, layer->Weights->pMass, width);
		}
		else
		{
			cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasTrans, height, width, 1, ALPHA, layer->Error->pMass, 1, layer->Input->pMass, 1, 1, layer->Weights->pMass, width);

			double norm = getNorm(layer->Weights->pMass, height*width);

			//if (norm > MAX_NORM)
			//{
				double k = MAX_NORM / norm;
				double* A = new double[height * 1];
				double* B = new double[1 * width];
				cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, height, width, 1, 0, A, 1, B, width, k, layer->Weights->pMass, width);
				delete A;
				delete B;
			//}
		}
	}
}
