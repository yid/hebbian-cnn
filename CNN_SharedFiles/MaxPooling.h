﻿#pragma once
#include "Layer.h"
namespace CNN_SharedFiles
{
	// класс субдискретизации
	class MaxPooling :
		public Layer
	{
	private:
		Volume* Output; // выход слоя
		int ReceptiveField_HEIGHT; // высота рецептивного поля
		int ReceptiveField_WIDTH; // ширина рецептивного поля
		Volume* BackError; // ошибка, распространяющаяся на предыдущий слой

	public:
		MaxPooling(int ReceptiveField_HEIGHT, int ReceptiveField_WIDTH);
		~MaxPooling();
		Volume* Forward(Volume* Input);
		Volume* Backward(Volume* Error);
		// связь с предыдущим слоем
		bool Connect(Size3D InputSize, bool iflearn);
		void Save(string folder, int layer_number); // ничего не делает
		MaxPooling(const MaxPooling & m);
		Layer* Copy();
	};
}
