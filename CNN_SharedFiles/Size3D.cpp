#include "Size3D.h"

namespace CNN_SharedFiles
{
	Size3D::Size3D(int Depth, int Height, int Width)
	{
		this->Width = Width;
		this->Height = Height;
		this->Depth = Depth;
	}
	Size3D::Size3D()
	{
	}
	Size3D::Size3D(const Size3D & s)
	{
		Width = s.Width;
		Height = s.Height;
		Depth = s.Depth;
	}
}
