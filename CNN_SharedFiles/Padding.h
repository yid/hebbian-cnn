#pragma once
#include "Layer.h"
namespace CNN_SharedFiles
{
	class Padding :public Layer
	{
	public:
		Volume * Output;
		Volume* BackError;
		int x; // padding for x-coordinate, width of output increases by 2*x pixels
		int y; // padding for y-coordinate, height of output increases by 2*y pixels
	
		Padding(int x, int y);
		~Padding();
		Volume* Forward(Volume* Input);
		Volume* Backward(Volume* Error);
		bool Connect(Size3D InputSize, bool iflearn);
		void Save(string folder, int layer_number);
		Padding(const Padding & l);
		Layer* Copy();
	};

}