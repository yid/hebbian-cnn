#include "DropoutNN.h"
#include <random>
#include <ctime>
#include <iostream>
using namespace std;
#include <typeinfo>


namespace CNN_SharedFiles
{
	void DropoutNN::AddToDropoutNN(Layer * nextlayer, double nextp)
	{
		p.push_back(nextp);
		NeuroNet::Add(nextlayer);
	}

	Volume * DropoutNN::Forward(Volume * Input)
	{
		if (iflearn)
		{
			Volume* Temp = Input;
			for (size_t i = 0; i < Layers.size(); i++)
			{

#pragma omp parallel for
				for (int j = 0; j < Temp->Size.Depth*Temp->Size.Height*Temp->Size.Width; j++)
				{
					if (!ThinnedNN[i][j])
						Temp->pMass[j] = 0;
				}
				Temp = Layers[i]->Forward(Temp);
			}
			Output = Temp;
			return Temp;
		}
		else
			return NeuroNet::Forward(Input);
	}

	double DropoutNN::TrainConstAlpha(Volume* Target, Volume* Input, double alpha, double maxnorm)
	{
		if (!iflearn)
		{
			cout << "This NeuroNet is not for learning.";
			return 0;
		}
		ThinnedNN = new bool*[this->Layers.size()];
		for (size_t i = 0; i < Layers.size(); i++)
		{
			int input_size = Layers[i]->InputSize.Depth*Layers[i]->InputSize.Height*Layers[i]->InputSize.Width;
			ThinnedNN[i] = new bool[input_size];
			bernoulli_distribution b(this->p[i]);
			srand((unsigned int)time(0));
			default_random_engine engine(rand());
			for (int j = 0; j < input_size; j++)
			{
				ThinnedNN[i][j]=b(engine);
			}
		}

		Volume* Temp = new Volume(*Input);
		double result = GradientNN::TrainConstAlpha(Target, Temp, alpha, maxnorm);

		delete Temp;
		for (size_t i = 0; i < Layers.size(); i++)
			delete[] ThinnedNN[i];
		delete[] ThinnedNN;
		return result;
	}

	void DropoutNN::FinishTraining()
	{
		iflearn = false;
		for (size_t i = 0; i < this->Layers.size(); i++)
		{
			if (typeid(*Layers[i]) == typeid(ConvLayer))
			{
				int size = dynamic_cast<ConvLayer*>(Layers[i])->K*dynamic_cast<ConvLayer*>(Layers[i])->S;
#pragma omp parallel for
				for (int j = 0; j < size; j++)
					dynamic_cast<ConvLayer*>(Layers[i])->Weights->pMass[j] *= p[i];
			}
			if (typeid(*Layers[i]) == typeid(FullConnectedLayer))
			{
				int size = dynamic_cast<FullConnectedLayer*>(Layers[i])->InputSize.Depth*dynamic_cast<FullConnectedLayer*>(Layers[i])->InputSize.Height*dynamic_cast<FullConnectedLayer*>(Layers[i])->InputSize.Width;
#pragma omp parallel for
				for (int j = 0; j < size; j++)
					dynamic_cast<FullConnectedLayer*>(Layers[i])->Weights->pMass[j] *= p[i];
			}
		}
	}

	DropoutNN::DropoutNN(Size3D inputSize, bool iflearn) :GradientNN(inputSize, iflearn)
	{
	}

	DropoutNN::DropoutNN(const DropoutNN & d) : GradientNN(d)
	{
		for (size_t i = 0; i < d.p.size(); i++)
			this->p.push_back(d.p[i]);
	}


	DropoutNN::~DropoutNN()
	{
		p.clear();
	}
}
