#include "Volume.h"

namespace CNN_SharedFiles
{
	Volume::Volume(Size3D Size)
	{
		this->Size = Size;
		pMass = new double[Size.Height*Size.Width*Size.Depth];
	}


	Volume::~Volume()
	{
		delete[] pMass;
	}
	double Volume::get(int z, int y, int x)
	{
		return pMass[z*Size.Width*Size.Height + y*Size.Width + x];
	}
	void Volume::set(int z, int y, int x, double value)
	{
		pMass[z*Size.Width*Size.Height + y*Size.Width + x] = value;
	}
	Volume::Volume(const Volume & volume)
	{
		this->Size = volume.Size;
		pMass = new double[Size.Height*Size.Width*Size.Depth];
#pragma omp parallel for
		for (int i = 0; i < Size.Height*Size.Width*Size.Depth; i++)
			pMass[i] = volume.pMass[i];
	}
}
