#pragma once
#include "Size3D.h"
namespace CNN_SharedFiles
{// ����� 4-������ ������, �������� ��� 3-������� �������
	class Size4D :
		public Size3D
	{
	public:
		int Depth4D;
		Size4D();
		Size4D(int Depth4D, int Depth, int Height, int Width);
		Size4D(const Size4D & s);
	};
}

