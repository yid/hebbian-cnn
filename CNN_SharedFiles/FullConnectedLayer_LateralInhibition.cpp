﻿#include "FullConnectedLayer_LateralInhibition.h"

namespace CNN_SharedFiles {

	// class for implementing a layer with lateral connections being trained with APEX algorithm (in Hebbian neural network)
	FullConnectedLayer_LateralInhibition::FullConnectedLayer_LateralInhibition(int NeuronsNumber, void(*ActivationFunction)(double* x, int size), void(*ActivationFunction1)(double* x, int size), bool SeqInhibition) : FullConnectedLayer(NeuronsNumber, ActivationFunction, ActivationFunction1)
	{
		this->SeqInhibition = SeqInhibition;
	}

	bool FullConnectedLayer_LateralInhibition::Connect(Size3D InputSize, bool iflearn) {
		LateralWeights = new double[outputSize.Height * outputSize.Height];
		for (int i = 0; i < outputSize.Height; i++)
		{
			for (int j = 0; j < outputSize.Height; j++) {
				if (i == j) {
					LateralWeights[i * outputSize.Height + j] = 0;
				}
				else {
					if (i > j) {
						LateralWeights[i * outputSize.Height + j] = 0;
					}
					else {
						LateralWeights[i * outputSize.Height + j] = 0;//-0.1 * (rand() / (double)RAND_MAX);
					}
				}
			}
		}
		return FullConnectedLayer::Connect(InputSize, iflearn);
	}
	Volume* FullConnectedLayer_LateralInhibition::Forward(Volume* Input) {
		FullConnectedLayer::Forward(Input);
		if (SeqInhibition) {
			for (int i = outputSize.Height - 1; i >= 0; i--) {
				for (int j = 0; j < outputSize.Height; j++) {
					Output->pMass[i] += LateralWeights[i*outputSize.Height + j] * Output->pMass[j];
				}
			}
		}
		else { // Может быть параллельным с помощью blas
			Volume* Inhibited = new Volume(outputSize);
			for (int i = outputSize.Height - 1; i >= 0; i--) {
				Inhibited->pMass[i] = 0;
				for (int j = 0; j < outputSize.Height; j++) {
					Inhibited->pMass[i] += LateralWeights[i*outputSize.Height + j] * Output->pMass[j];
				}
			}
			for (int i = outputSize.Height - 1; i >= 0; i--) {
				Output->pMass[i] += Inhibited->pMass[i];
			}
			delete Inhibited;
		}
		return Output;
	}


	FullConnectedLayer_LateralInhibition::~FullConnectedLayer_LateralInhibition()
	{
		delete[] LateralWeights;
	}

	Layer * FullConnectedLayer_LateralInhibition::Copy()
	{
		FullConnectedLayer_LateralInhibition* l = new FullConnectedLayer_LateralInhibition(*this);
		return l;
	}
}