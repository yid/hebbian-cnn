#pragma once
#include "GradientNN.h"
#include <string>
namespace CNN_SharedFiles
{
	// ��������������� �����, �������������� ��� �������� ���������� ����������
	class GradientNN_CheckGradients :
		public GradientNN
	{
	private:
		string Path; // ����, �� �������� ������������ ������� ����� �����
	public:
		GradientNN_CheckGradients(Size3D inputSize, bool iflearn, string path);
		~GradientNN_CheckGradients();
		double TrainConstAlpha(Volume* Target, Volume* Input, double alpha);
	protected:
		void TrainConvLayer(ConvLayer* layer);
		void TrainFCLayer(FullConnectedLayer* layer);
	};
}

