﻿#pragma once
#include "DatasetParser.h"
#include <string>
#include <iostream>
#include <fstream>
#include "Volume.h"
#include "bitmap_image.hpp"
using namespace std;

namespace CNN_SharedFiles
{
	DatasetParser::DatasetParser()
	{
		cifar10_names = new string[10]{ "airplane", "automobile", "bird", "cat", "deer", "dog", "frog", "horse", "ship", "truck" };
		CIFAR10Images = NULL;
		CIFAR10Answers = NULL;
		MNISTImages = NULL;
		MNISTAnswers = NULL;
	}


	DatasetParser::~DatasetParser()
	{
		delete[] cifar10_names;
	}

	int reverseInt(int i)
	{
		unsigned char c1, c2, c3, c4;
		c1 = i & 255, c2 = (i >> 8) & 255, c3 = (i >> 16) & 255, c4 = (i >> 24) & 255;
		return ((int)c1 << 24) + ((int)c2 << 16) + ((int)c3 << 8) + c4;
	};

	void DatasetParser::read_mnist(string full_path_images, string full_path_labels, int& number_of_images)
	{
		int image_size = mnist_image_height*mnist_image_width;
		unsigned char** images;
		unsigned char* labels;

		// reading images
		ifstream file_images(full_path_images, ios::binary);
		if (file_images.is_open())
		{
			int magic_number = 0, n_rows = 0, n_cols = 0;
			file_images.read((char *)&magic_number, sizeof(magic_number));
			magic_number = reverseInt(magic_number);
			if (magic_number != 2051) throw runtime_error("Invalid MNIST image file!");
			file_images.read((char *)&number_of_images, sizeof(number_of_images)), number_of_images = reverseInt(number_of_images);
			file_images.read((char *)&n_rows, sizeof(n_rows)), n_rows = reverseInt(n_rows);
			file_images.read((char *)&n_cols, sizeof(n_cols)), n_cols = reverseInt(n_cols);
			image_size = n_rows * n_cols;
			images = new unsigned char*[number_of_images];
			for (int i = 0; i < number_of_images; i++)
			{
				images[i] = new unsigned char[image_size];
				file_images.read((char *)images[i], image_size);
			}
			file_images.close();
		}
		else
			throw runtime_error("Cannot open file `" + full_path_images + "`!");

		// reading labels
		ifstream file_labels(full_path_labels, ios::binary);
		if (file_labels.is_open())
		{
			int magic_number = 0;
			file_labels.read((char *)&magic_number, sizeof(magic_number));
			magic_number = reverseInt(magic_number);
			if (magic_number != 2049) throw runtime_error("Invalid MNIST label file!");
			file_labels.read((char *)&number_of_images, sizeof(number_of_images)), number_of_images = reverseInt(number_of_images);
			labels = new unsigned char[number_of_images];
			for (int i = 0; i < number_of_images; i++)
				file_labels.read((char*)&labels[i], 1);
			file_labels.close();
		}
		else
			throw runtime_error("Unable to open file `" + full_path_labels + "`!");

		// converting to Volume
		MNISTImages = Char4DToVolume1D(images, number_of_images, mnist_image_depth, mnist_image_height, mnist_image_width);
		MNISTAnswers = Char1DToVolume1D(labels, number_of_images, mnist_classes_number);

		for (int i = 0; i < number_of_images; i++)
		{
			delete[] images[i];
		}
		delete[] images;
		delete[] labels;
	}

	void DatasetParser::read_cifar10(string full_path, int& number_of_images)
	{
		int image_size = cifar10_image_height*cifar10_image_width *cifar10_image_depth;
		unsigned char** cifar_images;
		unsigned char* cifar_labels;
		ifstream file(full_path, ios::binary);

		// reading data
		if (file.is_open())
		{
			cifar_images = new unsigned char*[number_of_images];
			cifar_labels = new unsigned char[number_of_images];
			for (int i = 0; i < number_of_images; i++)
			{
				cifar_images[i] = new unsigned char[image_size];
				file.read((char*)&cifar_labels[i], 1);
				file.read((char*)cifar_images[i], image_size);
			}
			file.close();
		}
		else
			throw runtime_error("Cannot open file `" + full_path + "`!");

		// converting to Volume
		CIFAR10Images = Char4DToVolume1D(cifar_images, number_of_images, cifar10_image_depth, cifar10_image_height, cifar10_image_width);
		CIFAR10Answers = Char1DToVolume1D(cifar_labels, number_of_images, cifar10_classes_number);

		for (int i = 0; i < number_of_images; i++)
			delete[] cifar_images[i];
		delete[] cifar_images;
		delete[] cifar_labels;
	}

	int DatasetParser::VolumeToBitmap(Volume * volume, string path)
	{
		bitmap_image bmp(volume->Size.Width, volume->Size.Height);
		for (int i = 0; i < volume->Size.Width; i++)
			for (int j = 0; j < volume->Size.Height; j++)
			{
				if (volume->Size.Depth == 3)
					bmp.set_pixel(i, j, volume->get(0, j, i) * 255, volume->get(1, j, i) * 255, volume->get(2, j, i) * 255);
				else if (volume->Size.Depth == 1)
					bmp.set_pixel(i, j, volume->get(0, j, i) * 255, volume->get(0, j, i) * 255, volume->get(0, j, i) * 255);
			}
		bmp.save_image(path);
		return 1;
	}

	Volume** DatasetParser::Char1DToVolume1D(unsigned char* arr, int array_size, int number_of_classes)
	{
		Volume** volume = new Volume*[array_size]; // TODO CAUTION this "new" doesn't have delete!
		for (int i = 0; i < array_size; i++)
		{
			volume[i] = new Volume(Size3D(1, number_of_classes, 1)); // TODO CAUTION this "new" doesn't have delete!
			for (int j = 0; j < number_of_classes; j++)
			{
				if (arr[i] == j)
					volume[i]->set(0, j, 0, 1);
				else
					volume[i]->set(0, j, 0, 0);
			}
		}
		return volume;
	}

	Volume** DatasetParser::Char4DToVolume1D(unsigned char** arr, int array_size, int depth, int height, int width)
	{
		Volume** volumes = new Volume*[array_size]; // TODO CAUTION this "new" doesn't have delete!
		for (int i = 0; i < array_size; i++)
		{
			volumes[i] = new Volume(Size3D(depth, height, width)); // TODO CAUTION this "new" doesn't have delete!
			for (int d = 0; d < depth; d++)
				for (int h = 0; h < height; h++)
					for (int w = 0; w < width; w++)
					{
						volumes[i]->set(d, h, w, arr[i][d*height*width + h*width + w] / (double)255);
					}
		}
		return volumes;
	}
}
