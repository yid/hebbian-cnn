﻿#include "NeuroNet.h"
#include"ConvLayer.h"
#include"FullConnectedLayer.h"
#include<iostream>
using namespace std;
#include "Size3D.h"
#include "fstream"
#include <string>
#include <typeinfo>
#include "windows.h"
#include <algorithm> // Для использования алгоритмов std::sort
#include <cmath>

namespace CNN_SharedFiles
{
	void NeuroNet::Backward(Volume * Error)
	{
		if (Output == 0)
			cout << "Output=0: do forward before doing backward.";
		else
			if (!iflearn)
				cout << "This NeuroNet is not for learning.";
			else
			{				
				Volume* Temp = Error;
				for (int i = Layers.size() - 1; i >= 0; i--)
				{
					Temp = Layers[i]->Backward(Temp);
				}
			}
	}

	NeuroNet::NeuroNet(Size3D inputSize, bool iflearn)
	{
		this->inputSize = inputSize;
		this->iflearn = iflearn;
		Output = 0;
	}

	void NeuroNet::Add(Layer * nextLayer)
	{
		bool connection = true;
		if (Layers.size() == 0)
		{
			connection = nextLayer->Connect(inputSize, iflearn);
			if (connection)
				nextLayer->FirstLayer = true;
		}
		else
			connection = nextLayer->Connect(Layers.back()->outputSize, iflearn);
		if (connection)
			Layers.push_back(nextLayer);
		else
			cout << "Layer " + to_string(Layers.size() - 1) + ": unsuccessful connection." << endl;
	}

	Volume * NeuroNet::Forward(Volume * Input)
	{
		Volume* Temp = Input;
		for (size_t i = 0; i < Layers.size(); i++)
			Temp = Layers[i]->Forward(Temp);
		Output = Temp;
		return Temp;
	}

	void NeuroNet::Save(string s)
	{
		if (s[s.size() - 1] != '\\')
			s = s + '\\';
		for (size_t i = 0; i < Layers.size(); i++)
		{
			Layers[i]->Save(s, i);
		}
	}

	bool NeuroNet::Load(string s)
	{
		/*try
		{
			vector<string> mFiles;
			if (s[s.size() - 1] != '\\')
				s = s + '\\';
			s += '*';

			WIN32_FIND_DATA FindFileData;
			HANDLE handle;
			handle = FindFirstFile(wstring(s.begin(), s.end()).c_str(), &FindFileData);
			if (handle != INVALID_HANDLE_VALUE)
			{
				do
				{
					wstring ws(FindFileData.cFileName);
					string s2(ws.begin(), ws.end());
					if (s2.size() > 3)
					{
						if (s2.substr(s2.size() - 4, 4) == ".cnn")
							mFiles.push_back(s.substr(s.size() - 1, 1) + s2);
					}
				} while (FindNextFile(handle, &FindFileData) != 0);
				FindClose(handle);
			}

			sort(mFiles.begin(), mFiles.end());

			for (int i = 0; i < mFiles.size(); i++)
			{
				string sLayer = mFiles[i].substr(2, 2);
				Layer * nextlayer;
				if (sLayer == "FC")
				{
					nextlayer=new FullConnectedLayer()
				}
				if (sLayer == "CL")
				{

				}
			}
		}
		catch ()
		{
			return false;
		}*/
		return false;
	}

	NeuroNet::~NeuroNet()
	{
		for (size_t i = 0; i < Layers.size(); i++)
			delete Layers[i];
		Layers.clear();
	}
	NeuroNet::NeuroNet(const NeuroNet & n)
	{
		this->iflearn = n.iflearn;
		this->inputSize = n.inputSize;
		this->Output = 0;
		for (size_t i = 0; i < n.Layers.size(); i++)
			this->Layers.push_back(n.Layers[i]->Copy());
	}

	double NeuroNet::Evaluate_l1(Volume ** testInputs, Volume ** testAnswers, int test_images_number)
	{
		double err = 0;
		for (int i = 0; i < test_images_number; i++) {
			double temp = 0;
			Volume* ans = this->Forward(testInputs[i]);
			for (int j = 0; j < ans->Size.Depth*ans->Size.Height *ans->Size.Width; j++) {
				temp += abs(ans->pMass[j] - testAnswers[i]->pMass[j]);
			}
			err = (i * err + temp) / (i + 1);
		}
		return err;
	}

	double NeuroNet::getNorm(double * vector, int size)
	{
		double norm = 0;
		int threads = 4;
		double* mas = new double[threads];
		for (int i = 0; i < threads; i++)
			mas[i] = 0;
#pragma omp parallel for
		for (int i = 0; i < threads; i++)
		{
			for (int j = i*size / 4; j < (i + 1)*size / 4; j++)
				mas[i] += pow(vector[j], 2);
		}
		for (int i = 0; i < threads; i++)
			norm += mas[i];
		norm = sqrt(norm);
		delete mas;
		return norm;
	}
}