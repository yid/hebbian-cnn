#pragma once
#include "GradientNN.h"
#include<vector>
namespace CNN_SharedFiles
{
	// ����� ���������, ������������ ��� �������� dropout-�������������; ��������� �� ����������� ���������
	class DropoutNN :
		public GradientNN
	{
	private:
		vector<double> p; // ������ dropout-���������� ��������������� ������� ����
		bool** ThinnedNN; // ������ �������� ��������, ��������������� ������� �����; ������ �������� ����������, ������������ �� ������������ ������ � ��������� ������ ���������; ������������ ��� ������ ��������� ��������

	public:
		void AddToDropoutNN(Layer* nextlayer, double nextp);

		Volume* Forward(Volume* Input);

		double TrainConstAlpha(Volume* Target, Volume* Input, double alpha, double maxnorm);

		void FinishTraining(); // ������� ��������� ��������, � ������� ����������� dropout-�������������

		DropoutNN(Size3D inputSize, bool iflearn);

		DropoutNN(const DropoutNN & d);

		~DropoutNN();
	};
}

