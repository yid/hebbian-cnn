﻿#pragma once
#include"Size3D.h"
namespace CNN_SharedFiles
{
	// класс представляет функционал для хранения и работы с 3-мерными матрицами 
	class Volume
	{

	public:
		Size3D Size;
		double * pMass;

		Volume(Size3D Size);
		~Volume();
		double get(int z, int y, int x);
		void set(int z, int y, int x, double value);
		Volume(const Volume & volume);
	};
}

