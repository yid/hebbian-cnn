﻿#pragma once
#include"Layer.h"
#include<vector>
using namespace std;

namespace CNN_SharedFiles {
	// класс нейронной сети
	class NeuroNet
	{
	protected:
		Size3D inputSize; // размер входа
		Volume* Output; // выход нейросети
		bool iflearn; // флаг, обозначающий режим функционирования (обучение или работа)
		void Backward(Volume* Error); // функция обратного распространения нейросети

	public:
		static double getNorm(double* vector, int size); // функция нахождения нормы вектора в декартовом пространстве // TODO must be protected, public just for testing

		vector<Layer*> Layers; // вектор слоев // TODO must be protected, public just for testing

		NeuroNet(Size3D inputSize, bool iflearn);

		void Add(Layer* nextLayer); // функция добавления слоя в нейросеть

		virtual Volume* Forward(Volume* Input); // функция прямого распространения нейросети

		void Save(string s); // функция сохранения нейросети

		bool Load(string s);// нерабочий вариант

		virtual ~NeuroNet();

		NeuroNet(const NeuroNet & n);
		
		/**
		computes mean abs error (l1) for a neuronet
		@param testInputs dynamic array of test input images represented as Volume*
		@param testAnswers dynamic array of answer vectors for every input
		@param test_images_number number of elements in this array
		@return mean abs error
		*/
		double Evaluate_l1(Volume** testInputs, Volume** testAnswers, int test_images_number);
	};

}