#include "Layer.h"
#include<iostream>
using namespace std;
#include<cmath>

namespace CNN_SharedFiles
{
	bool Layer::Connect(Size3D InputSize, bool iflearn)
	{
		this->ifconnect = true;
		this->InputSize = InputSize;
		this->iflearn = iflearn;
		return true;
	}
	Layer::Layer()
	{
		ifconnect = false;
	}
	Layer::Layer(const Layer & l)
	{
		this->FirstLayer = l.FirstLayer;
		this->ifconnect = l.ifconnect;
		this->iflearn = l.iflearn;
		this->InputSize = l.InputSize;
		this->outputSize = l.outputSize;
	}

	void Layer::Sigmoid(double* x, int size)
	{
		double beta = 1;
#pragma omp parallel for
		for (int i = 0; i < size; i++)
			x[i] = 1 / (1 + exp(-beta*x[i]));
	}

	void Layer::Sigmoid1(double* x, int size)
	{
		double beta = 1;
#pragma omp parallel for
		for (int i = 0; i < size; i++)
			x[i] = beta*exp(-beta*x[i]) / pow(1 + exp(-beta*x[i]), 2);
	}

	void Layer::Sigmoid_Antisimmetric(double* x, int size)
	{
		double beta = 1;
#pragma omp parallel for
		for (int i = 0; i < size; i++)
			x[i] = 2 / (1 + exp(-beta*x[i])) - 1;
	}

	void Layer::Sigmoid_Antisimmetric1(double* x, int size)
	{
		double beta = 1;
#pragma omp parallel for
		for (int i = 0; i < size; i++)
			x[i] = 2 * beta*exp(-beta*x[i]) / pow(1 + exp(-beta*x[i]), 2);
	}

	void Layer::ReLU(double* x, int size)
	{
#pragma omp parallel for
		for (int i = 0; i < size; i++)
			if (x[i] < 0)
				x[i] = 0;
	}

	void Layer::ReLU1(double* x, int size)
	{
#pragma omp parallel for
		for (int i = 0; i < size; i++)
			if (x[i] > 0)
				x[i] = 1;
			else
				x[i] = 0;
	}

	void Layer::Softmax(double* x, int size)
	{
		double sum = 0;
		for (int i = 0; i < size; i++)
			sum += exp(x[i]);
#pragma omp parallel for
		for (int i = 0; i < size; i++)
			x[i] = exp(x[i]) / sum;
	}

	void Layer::Softmax1(double* x, int size)
	{
		double sum = 0;
		for (int i = 0; i < size; i++)
			sum += exp(x[i]);
#pragma omp parallel for
		for (int i = 0; i < size; i++)
			x[i] = (exp(x[i])*(sum - exp(x[i]))) / (sum*sum);
	}

	void Layer::Linear(double* x, int size)
	{
	}

	void Layer::Linear1(double* x, int size)
	{
#pragma omp parallel for
		for (int i = 0; i < size; i++)
			x[i] = 1;
	}
	void Layer::HardMax(double* x, int size) {
		int MaxIndex = 0;
		for (int i = 1; i < size; i++) {
			if (x[i] > x[MaxIndex]) {
				x[MaxIndex] = 0;
				MaxIndex = i;
			}
			else
				x[i] = 0;
		}
		if (x[MaxIndex] == x[MaxIndex])
			x[MaxIndex] = 1;
	}
}

