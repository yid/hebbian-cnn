#include "MaxPooling.h"


namespace CNN_SharedFiles
{
	MaxPooling::MaxPooling(int ReceptiveField_HEIGHT, int ReceptiveField_WIDTH)
	{
		this->ReceptiveField_HEIGHT = ReceptiveField_HEIGHT;
		this->ReceptiveField_WIDTH = ReceptiveField_WIDTH;
	}


	MaxPooling::~MaxPooling()
	{
		delete Output;
		if (iflearn)
		{
			delete BackError;
		}
	}

	Volume * MaxPooling::Forward(Volume * Input)
	{
		this->Input = Input;
#pragma omp parallel for
		for (int k = 0; k < InputSize.Depth; k++)
			for (int i = 0; i < outputSize.Height; i++)
				for (int j = 0; j < outputSize.Width; j++)
				{
					double max = 0;
					for (int l = 0; l < ReceptiveField_HEIGHT; l++)
						for (int m = 0; m < ReceptiveField_WIDTH; m++)
						{
							double value = Input->get(k, i*ReceptiveField_HEIGHT + l, j*ReceptiveField_WIDTH + m);
							if ((m == 0) && (l == 0))
								max = value;
							else
								if (value > max)
									max = value;
						}
					Output->set(k, i, j, max);
				}
		return Output;
	}

	Volume * MaxPooling::Backward(Volume * Error)
	{
#pragma omp parallel for
		for (int k = 0; k < InputSize.Depth; k++)
			for (int i = 0; i < outputSize.Height; i++)
				for (int j = 0; j < outputSize.Width; j++)
					for (int l = 0; l < ReceptiveField_HEIGHT; l++)
						for (int m = 0; m < ReceptiveField_WIDTH; m++)
							BackError->set(k, i*ReceptiveField_HEIGHT + l, j*ReceptiveField_WIDTH + m, Error->get(k, i, j) / (ReceptiveField_HEIGHT*ReceptiveField_WIDTH));
#pragma omp parallel for
		for (int k = 0; k < InputSize.Depth; k++)
			for (int j = outputSize.Height*ReceptiveField_HEIGHT; j < InputSize.Height; j++)
				for (int i = 0; i < InputSize.Width; i++)
				{
					BackError->set(k, j, i, BackError->get(k, outputSize.Height*ReceptiveField_HEIGHT - 1, i));
				}
#pragma omp parallel for
		for (int k = 0; k < InputSize.Depth; k++)
			for (int j = 0; j < InputSize.Height; j++)
				for (int i = outputSize.Width*ReceptiveField_WIDTH; i < InputSize.Width; i++)
					BackError->set(k, j, i, BackError->get(k, j, outputSize.Width*ReceptiveField_WIDTH - 1));

		return BackError;
	}

	bool MaxPooling::Connect(Size3D InputSize, bool iflearn)
	{
		Layer::Connect(InputSize, iflearn);
		outputSize.Height = InputSize.Height / ReceptiveField_HEIGHT;
		outputSize.Width = InputSize.Width / ReceptiveField_WIDTH;
		outputSize.Depth = InputSize.Depth;
		Output = new Volume(outputSize);
		if (iflearn)
		{
			BackError = new Volume(InputSize);
		}
		return true;
	}

	void MaxPooling::Save(string folder, int layer_number)
	{
	}

	MaxPooling::MaxPooling(const MaxPooling & m):Layer(m)
	{

		Output = new Volume(outputSize);
		if (iflearn)
		{
			BackError = new Volume(InputSize);
		}
		this->ReceptiveField_HEIGHT = m.ReceptiveField_HEIGHT;
		this->ReceptiveField_WIDTH = m.ReceptiveField_WIDTH;
	}

	Layer * MaxPooling::Copy()
	{
		Layer* l = new MaxPooling(*this);
		return l;
	}
}