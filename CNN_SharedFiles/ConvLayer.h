﻿#pragma once
#include "Layer.h"
#include "Volume4D.h"

namespace CNN_SharedFiles
{
	// класс сверточного слоя, дочерний для класса Layer
	class ConvLayer :
		public Layer
	{
	private:
		void(*ActivationFunction)(double* x, int size); // активационная функция слоя
		void(*ActivationFunction1)(double* x, int size); // производная активационной функции слоя
		Volume* BackError; // ошибка, распространяющаяся на предыдущий слой
	public:
		Volume* UnactivatedOutput; //  неактивированный выход слоя
		Volume* Output; // выход слоя
		int ReceptiveField_HEIGHT; // высота рецептивного поля
		int ReceptiveField_WIDTH; // ширина рецептивного поля
		int Stride_X; // шаг смещения фильтра по горизонтали
		int Stride_Y; // шаг смещения фильтра по вертикали
		int K; // количество нейронов (ядер) слоя
		int L; // размер выходного вектора (для одного ядра)
		int S; // размер ядра
		double* TransformedInput; // вход, разложенный по шагам смещения фильтра; состоит из <ширина_выхода*высота_выхода> векоров размером <ширина_фильтра*высота_фильтра*глубина_входа>, расположенных друг за другом, учитывая <шаг_смещения_фильтра> (т.е. могут быть повторения).
		Volume4D* Weights; // матрица, в строках которой веса ядра

		ConvLayer(int KernelsNumber, int ReceptiveField_HEIGHT, int ReceptiveField_WIDTH, int Stride_X, int Stride_Y, void(*ActivationFunction)(double* x, int size), void(*ActivationFunction1)(double* x, int size));

		ConvLayer(string s, int Stride_X, int Stride_Y, void(*ActivationFunction)(double* x, int size), void(*ActivationFunction1)(double* x, int size), bool iflearn);

		bool Connect(Size3D InputSize, bool iflearn);

		Volume* Forward(Volume* Input);

		Volume* Backward(Volume* Error);

		~ConvLayer();

		void Save(string folder, int layer_number);

		ConvLayer(const ConvLayer & c);

		Layer* Copy();
	};

}