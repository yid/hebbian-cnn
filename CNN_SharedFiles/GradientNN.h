﻿#pragma once
#include "NeuroNet.h"
#include "ConvLayer.h"
#include "FullConnectedLayer.h"

namespace CNN_SharedFiles
{ 
	// наследник класса нейросети, использующий для обучения метод градиентного спуска (ОСНОВНОЙ РАБОЧИЙ ВАРИАНТ)
	class GradientNN :
		public NeuroNet
	{
	public:
		double ALPHA; // параметр обучения
		int TrainCounter; // количество проведенных обучений
		double MAX_NORM=-1; // модификатор, обозначающий, будет ли использована MaxNorm-регуляризация; если<0, то не будет; если>0, то равняется ограничивающей постоянной 

		static double Exponential(double x); // экспоненциальная функция с определенной амплитудой и смещением
	
		enum ChangeAlpha{ExponentialFunction}; // перечисление способов изменения параметра обучения при квазистационарном подходе

		GradientNN(Size3D inputSize, bool iflearn);

		virtual ~GradientNN();

		GradientNN(const GradientNN & g);

		virtual double TrainConstAlpha(Volume* Target, Volume* Input,double alpha, double maxnorm); // Обучение со стационарным параметром обучения

		double TrainQuasiConstAlpha(Volume* Target, Volume* Input, ChangeAlpha func); // Обучение с квазистационарным параметром обучения

		double TrainQuasiAnaliticAlpha(Volume* Target, Volume* Input); // обучение с адаптивным параметром, выбирающимся в соответствии с квазианалитической оптимизацией

		double TrainDichotomy(Volume* Target, Volume* Input); // обучение с адаптивным параметром, выбирающимся в соответствии с оптимизацией методом дихотомии

		void Jog(double x);	// встряска весов с амплитудой x
	protected:
		virtual void TrainConvLayer(ConvLayer* layer); // функция обучения сверточного слоя
		virtual void TrainFCLayer(FullConnectedLayer* layer); // функция обучения полносвязного слоя
	};
}

