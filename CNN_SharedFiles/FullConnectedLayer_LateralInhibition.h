#pragma once
#include "FullConnectedLayer.h"
#include "Volume.h"

namespace CNN_SharedFiles {
	class FullConnectedLayer_LateralInhibition :public FullConnectedLayer
	{
	public:
		FullConnectedLayer_LateralInhibition(int NeuronsNumber, void(*ActivationFunction)(double* x, int size), void(*ActivationFunction1)(double* x, int size), bool SeqInhibition);
		~FullConnectedLayer_LateralInhibition();

		bool Connect(Size3D InputSize, bool iflearn);
		Volume* Forward(Volume* Input);

		bool SeqInhibition = false;
		double* LateralWeights;

		Layer * Copy();
	};

}