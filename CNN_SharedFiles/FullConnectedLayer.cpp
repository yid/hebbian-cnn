﻿#include "FullConnectedLayer.h"
#include <iostream>
using namespace std;
#include"cblas.h"
#include <fstream>

namespace CNN_SharedFiles
{
	FullConnectedLayer::FullConnectedLayer(int NeuronsNumber, void(*ActivationFunction)(double* x, int size), void(*ActivationFunction1)(double* x, int size))
	{
		outputSize.Depth = 1;
		outputSize.Width = 1;
		outputSize.Height = NeuronsNumber;
		this->ActivationFunction = ActivationFunction;
		this->ActivationFunction1 = ActivationFunction1;
		Output = new Volume(outputSize);
	}

	bool FullConnectedLayer::Connect(Size3D InputSize, bool iflearn)
	{
		if (ifconnect)
		{
			if ((this->InputSize.Depth == InputSize.Depth) && (this->InputSize.Height == InputSize.Height) && (this->InputSize.Width == InputSize.Width))
				return true;
			else
				return false;
		}
		else
		{
			Layer::Connect(InputSize, iflearn);
			double a = -0.1, b = 0.1;
			Weights = new Volume(Size3D(1, outputSize.Height, InputSize.Width*InputSize.Height*InputSize.Depth));
#pragma omp parallel for
			for (int i = 0; i < Weights->Size.Depth*Weights->Size.Height*Weights->Size.Width; i++)
				Weights->pMass[i] = (b - a)*rand() / RAND_MAX + a;
			UnactivatedOutput = new Volume(outputSize);
			if (iflearn)
			{
				BackError = new Volume(InputSize);
			}
			return true;
		}
	}

	Volume * FullConnectedLayer::Forward(Volume * Input)
	{
		this->Input = Input;
		// параллельное умножение UnactivatedOutput = Weights * Input
		cblas_dgemv(CblasRowMajor, CblasNoTrans, outputSize.Height, InputSize.Width*InputSize.Height*InputSize.Depth, 1.0, Weights->pMass, InputSize.Width*InputSize.Height*InputSize.Depth, Input->pMass, 1, 0, UnactivatedOutput->pMass, 1);

#pragma omp parallel for
		for (int i = 0; i < UnactivatedOutput->Size.Depth*UnactivatedOutput->Size.Height*UnactivatedOutput->Size.Width; i++)
			Output->pMass[i] = UnactivatedOutput->pMass[i];
		ActivationFunction(Output->pMass, UnactivatedOutput->Size.Depth*UnactivatedOutput->Size.Height*UnactivatedOutput->Size.Width);
		return Output;
	}

	Volume * FullConnectedLayer::Backward(Volume * Error)
	{
		this->Error = Error;
		//Error=Error*f'(UnactivatedOutput) поэлементно
		double* Temp = new double[outputSize.Depth*outputSize.Height*outputSize.Width];
#pragma omp parallel for
		for (int i = 0; i < outputSize.Depth*outputSize.Height*outputSize.Width; i++)
			Temp[i] = UnactivatedOutput->pMass[i];
		ActivationFunction1(Temp, outputSize.Depth*outputSize.Height*outputSize.Width);
#pragma omp parallel for
		for (int i = 0; i < outputSize.Depth*outputSize.Height*outputSize.Width; i++)
			Error->pMass[i] = Error->pMass[i] * Temp[i];
		delete Temp;

		if (FirstLayer)
			return 0;
		else
		{
			// Параллельное умножение BackError = (Weights^T)*Error
			int m = InputSize.Width*InputSize.Height*InputSize.Depth;
			int n = outputSize.Height;
			cblas_dgemv(CblasRowMajor, CblasTrans, n, m, 1.0, Weights->pMass, m, Error->pMass, 1, 0, BackError->pMass, 1);
			return BackError;
		}
	}

	FullConnectedLayer::~FullConnectedLayer()
	{
		delete Output;
		delete UnactivatedOutput;
		if (ifconnect)
		{
			delete Weights;
		}
		if (iflearn)
		{
			delete BackError;
		}
	}

	void FullConnectedLayer::Save(string folder, int layer_number)
	{
		string full_address = "";

		if (layer_number < 10)
			full_address = folder + '0' + to_string(layer_number) + "FC.cnn";
		else
			full_address = folder + to_string(layer_number) + "FC.cnn";

		ofstream out(full_address);
		out << outputSize.Height << endl << InputSize.Depth << endl << InputSize.Height << endl << InputSize.Width << endl;
		for (int i = 0; i < Weights->Size.Depth*Weights->Size.Height*Weights->Size.Width; i++)
			out << Weights->pMass[i] << endl;
		out.close();
	}

	FullConnectedLayer::FullConnectedLayer(string s, void(*ActivationFunction)(double* x, int size), void(*ActivationFunction1)(double* x, int size), bool iflearn)
	{
		ifstream infile(s);
		outputSize.Depth = 1;
		outputSize.Width = 1;
		infile >> outputSize.Height;

		this->ActivationFunction = ActivationFunction;
		this->ActivationFunction1 = ActivationFunction1;
		Output = new Volume(outputSize);

		int inputdepth, inputheight, inputwidth;
		infile >> inputdepth >> inputheight >> inputwidth;
		Layer::Connect(Size3D(inputdepth, inputheight, inputwidth), iflearn);

		Weights = new Volume(Size3D(1, outputSize.Height, InputSize.Width*InputSize.Height*InputSize.Depth));
		for (int i = 0; i < Weights->Size.Depth*Weights->Size.Height*Weights->Size.Width; i++)
			infile >> Weights->pMass[i];

		UnactivatedOutput = new Volume(outputSize);
		if (iflearn)
		{
			BackError = new Volume(InputSize);
		}
	}
	FullConnectedLayer::FullConnectedLayer(const FullConnectedLayer & f) :Layer(f)
	{
		this->ActivationFunction = f.ActivationFunction;
		this->ActivationFunction1 = f.ActivationFunction1;
		UnactivatedOutput = new Volume(outputSize);
		if (iflearn)
		{
			BackError = new Volume(InputSize);
		}
		Output = new Volume(outputSize);
		Weights = new Volume(Size3D(1, outputSize.Height, InputSize.Width*InputSize.Height*InputSize.Depth));
#pragma omp parallel for
		for (int i = 0; i < Weights->Size.Depth*Weights->Size.Height*Weights->Size.Width; i++)
			Weights->pMass[i] = f.Weights->pMass[i];
	}
	Layer * FullConnectedLayer::Copy()
	{
		FullConnectedLayer* l = new FullConnectedLayer(*this);
		return l;
	}
}

