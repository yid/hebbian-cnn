﻿#pragma once
#include "NeuroNet.h"
#include "ConvLayer.h"
#include "FullConnectedLayer.h"
#include "FullConnectedLayer_LateralInhibition.h"

namespace CNN_SharedFiles
{
	class HebbianNN :public NeuroNet
	{
	public:
		enum TrainingRule { Hebb, Oja_1, Oja_n, Oja_n_theory, Oja_1_Activated, Oja_n_Activated, Oja_n_theory_Activated, APEX_Activated};
		enum SupervisedRule {NotSupervised, DirectionLast, DirectionAll, TargetedLast, GradientLastDirectionAll, GradientLast};
		HebbianNN(Size3D inputSize, bool iflearn);
		virtual ~HebbianNN();
		virtual double TrainConstAlpha(Volume* Input, Volume* Target, double alpha, TrainingRule rule, SupervisedRule Srule, double MAX_NORM=-1); // обучение с постоянной скоростью; MAX_NORM для нормализации при обучении по правилу Хебба; MAX_NORM is used if it is > 0.
		HebbianNN(const HebbianNN & g);
	private:
		double ALPHA; // скорость обучения
		virtual void TrainFCLayer(FullConnectedLayer* layer, TrainingRule rule, double MAX_NORM=-1); // функция обучения полносвязного слоя; MAX_NORM is used if it is > 0.
		virtual void TrainFCLayerHebb(FullConnectedLayer* layer, bool Activated, double MAX_NORM=-1); // функция обучения полносвязного слоя по классическому правилу Хебба с нормализацией по норме MAX_NORM; MAX_NORM is used if it is > 0.
		virtual void TrainFCLayerOja_1(FullConnectedLayer* layer, bool Activated); // функция обучения полносвязного слоя по правилу Oja_1; MAX_NORM is used if it is > 0.
		virtual void TrainFCLayerOja_n(FullConnectedLayer* layer, bool Activated, bool theory); // функция обучения полносвязного слоя по правилу Oja_n
		virtual void TrainConvLayer(ConvLayer* layer, TrainingRule rule, double MAX_NORM=-1); // learning function of convolutional layer; MAX_NORM is used if it is > 0.
		virtual void TrainConvLayerHebb(ConvLayer* layer, double MAX_NORM=-1); // learning function of convolutional layer using classical Hebb rule with normalization with norm MAX_NORM; MAX_NORM is used if it is > 0.
		virtual void TrainConvLayerOja_1(ConvLayer* layer, double MAX_NORM = -1); // learning function of convolutional layer using Oja_1 rule; MAX_NORM is used if it is > 0.
		virtual void TrainConvLayerOja_n(ConvLayer* layer, bool Activated, bool Theory); // learning function of convolutional layer using Oja_n rule

		virtual void TrainFCLayerGradient(FullConnectedLayer* layer, Volume* error, double MAX_NORM = -1);// learning function of full-connected layer using gradient decay rule; MAX_NORM is used for normalization if it is > 0.
		
		virtual void TrainFCLateralLayerAPEX(FullConnectedLayer_LateralInhibition* layer, bool Activated);

		virtual void TrainFCLayer_SupervisedWithDirection(FullConnectedLayer* layer, bool PositiveDirection, TrainingRule rule, double MAX_NORM);
		virtual void TrainFCLayer_SupervisedWithTarget(FullConnectedLayer* layer, Volume* Target, TrainingRule rule, double MAX_NORM);
	};
}
