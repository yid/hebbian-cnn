#include "Padding.h"


namespace CNN_SharedFiles
{
	Padding::Padding(int x, int y)
	{
		this->x = x;
		this->y = y;
	}

	Padding::~Padding()
	{
		delete Output;
		if (iflearn)
		{
			delete BackError;
		}
	}

	Volume * Padding::Forward(Volume * Input)
	{
		this->Input = Input;
#pragma omp parallel for
		for (int k = 0; k < outputSize.Depth; k++)
			for (int h = 0; h < outputSize.Height; h++)
				for (int w = 0; w < outputSize.Width; w++)
				{
					if ((h >= y) && (h < outputSize.Height - y) && (w >= x) && (w < outputSize.Width - x))
						Output->set(k, h, w, Input->get(k, h - y, w - x));
					else
						Output->set(k, h, w, 0);
				}
		return Output;
	}

	Volume * Padding::Backward(Volume * Error)
	{
#pragma omp parallel for
		for (int k = 0; k < InputSize.Depth; k++)
			for (int h = 0; h < InputSize.Height; h++)
				for (int w = 0; w < InputSize.Width; w++)
				{
					BackError->set(k, h, w, Error->get(k, h + y, w + x));
				}
		return BackError;
	}

	bool Padding::Connect(Size3D InputSize, bool iflearn)
	{
		Layer::Connect(InputSize, iflearn);
		outputSize.Height = InputSize.Height + 2 * y;
		outputSize.Width = InputSize.Width + 2 * x;
		outputSize.Depth = InputSize.Depth;
		Output = new Volume(outputSize);
		if (iflearn)
		{
			BackError = new Volume(InputSize);
		}
		return true;
	}

	void Padding::Save(string folder, int layer_number)
	{
	}

	Padding::Padding(const Padding & p)
	{
		Output = new Volume(outputSize);
		if (iflearn)
		{
			BackError = new Volume(InputSize);
		}
		this->x = p.x;
		this->y = p.y;
	}

	Layer * Padding::Copy()
	{
		Layer* l = new Padding(*this);
		return l;
	}
}