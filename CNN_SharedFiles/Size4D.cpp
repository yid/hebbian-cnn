#include "Size4D.h"


namespace CNN_SharedFiles
{
	Size4D::Size4D() :Size3D()
	{
	}
	Size4D::Size4D(int Depth4D, int Depth, int Height, int Width) : Size3D(Depth, Height, Width)
	{
		this->Depth4D = Depth4D;
	}
	Size4D::Size4D(const Size4D & s):Size3D(s)
	{
		Depth4D = s.Depth4D;
	}
}
